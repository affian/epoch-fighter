/**
 * 
 */
package net.aeon.epoch;

import java.util.HashMap;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.SpriteSheetFont;

/**
 * Singleton resource manager for game assets
 * 
 * currently all assets are hard coded in requiring a new line of code for each
 * asset added. This will need fixing so that all assets can be stored in the
 * Jar file with a more flexible system for adding new assets.
 * 
 * @author Affian
 */
public class ResourceManager {
	
	private static final String DIR = "/resources/";
	private static final Color GREEN = new Color(191, 220, 191);
	private static ResourceManager instance = null;

	private HashMap<String, Image> images;
	private HashMap<String, MetaAnimation> animations;
	private HashMap<String, Sound> sounds;
	private HashMap<String, Music> music;
	private HashMap<String, SpriteSheetFont> fonts;
	private HashMap<String, SpriteSheet> spriteSheets;
	
	public static ResourceManager getInstance() {
		if (instance == null) {
			try {
				instance = new ResourceManager();
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
		return instance;
	}
	
	/**
	 * @throws SlickException
	 */
	private ResourceManager() throws SlickException {
		images = new HashMap<String, Image>();
		sounds = new HashMap<String, Sound>();
		music = new HashMap<String, Music>();
		fonts = new HashMap<String, SpriteSheetFont>();
		spriteSheets = new HashMap<String, SpriteSheet>();
		animations = new HashMap<String, MetaAnimation>();
		
		loadImages();
		loadSounds();
		loadMusic();
		loadFonts();
		loadSpriteSheets();
		
		//Must be last
		loadAnimations();
	}
	
	/**
	 * Hard coded list of Images to load
	 * @throws SlickException
	 */
	private void loadImages() throws SlickException {
		
		
		images.put("laser", new Image(DIR + "images/laser.png"));
		images.put("titlecard", new Image(DIR + "images/titlecard.png"));
		//images.put("bomb", new Image(DIR + "images/bomb.png", green));
		images.put("borderleft", new Image(DIR + "images/borderleft.png"));
		images.put("borderright", new Image(DIR + "images/borderright.png"));
		images.put("bullet1", new Image(DIR + "images/bullet1.png", GREEN));
		images.put("bullet2", new Image(DIR + "images/bullet2.png", Color.magenta));
		images.put("creeper-left", new Image(DIR + "images/creeper-left.png"));
		images.put("enemy1", new Image(DIR + "images/enemy1.png", Color.black));
		images.put("enemy2", new Image(DIR + "images/enemy2.png", GREEN));
		images.put("green-hammer", new Image(DIR + "images/green-hammer.png", GREEN));
		images.put("green-armour", new Image(DIR + "images/green-armour.png", GREEN));
		images.put("miku-right", new Image(DIR + "images/miku-right.png"));
		images.put("powerup", new Image(DIR + "images/powerup.png", GREEN));
		images.put("star1", new Image(DIR + "images/star1.png", Color.black));
		images.put("star2", new Image(DIR + "images/star2.png", Color.black));
		images.put("star3", new Image(DIR + "images/star3.png", Color.black));
		images.put("star4", new Image(DIR + "images/star4.png", Color.black));
		images.put("textBackdrop", new Image(DIR + "images/textBackdrop.png"));
		images.put("ground", new Image(DIR + "images/ground.png"));
		images.put("gameover", new Image(DIR + "images/gameover.png"));
		images.put("characterSelectBackground", new Image(DIR + "images/characterSelectBackground.png"));
		images.put("characterSelectTitle", new Image(DIR + "images/characterSelectTitle.png"));
	}
	
	/**
	 * Hard coded list of Animations to load
	 * @throws SlickException
	 */
	private void loadAnimations() throws SlickException {
		animations.put("spinner", new MetaAnimation(this.getSpriteSheet("spritesheet2"), 75, 0, 6, 8));
		animations.put("greenFactory", new MetaAnimation(new SpriteSheet(DIR + "images/greenFactory.png", 48, 56), 200, 3));
		animations.put("waterfall", new MetaAnimation(new SpriteSheet(DIR + "images/waterfall.png", 72, 56), 150, 4));
		animations.put("greenGround1", new MetaAnimation(new SpriteSheet(DIR + "images/greenGround1.png", 24, 28), 75, 6));
		animations.put("boober", new MetaAnimation(this.getSpriteSheet("spritesheet2"), 75, 3, 4, 5));
		animations.put("uship", new MetaAnimation(this.getSpriteSheet("spritesheet2"), 75, 0, 5, 8));
		animations.put("greenGround2left", new MetaAnimation(new SpriteSheet(DIR + "images/greenGround2left.png", 48, 28), 75, 3));
		animations.put("greenGround2right", new MetaAnimation(new SpriteSheet(DIR + "images/greenGround2right.png", 48, 28), 75, 3));
		animations.put("1k-gem", new MetaAnimation(new SpriteSheet(DIR + "images/1k-gem.png", 13, 23, GREEN), 75, 6));
		
	}
	
	/**
	 * Hard coded list of Sounds to load
	 * @throws SlickException
	 */
	private void loadSounds() throws SlickException {
		sounds.put("shoot", new Sound(DIR + "sounds/shoot.ogg"));
		sounds.put("typeBlip", new Sound(DIR + "sounds/typeBlip.ogg"));
		sounds.put("menu", new Sound(DIR + "sounds/menu.ogg"));
		sounds.put("select", new Sound(DIR + "sounds/select.ogg"));
	}
	
	/**
	 * Hard coded list of Music to load
	 * @throws SlickException
	 */
	private void loadMusic() throws SlickException {
		//music.put("Rock Garden", new Music(DIR + "sounds/Rock Garden.ogg", true));
	}
	
	/**
	 * Hard coded list of Fonts to load
	 * @throws SlickException
	 */
	private void loadFonts() throws SlickException {
		fonts.put("default", new SpriteSheetFont(new SpriteSheet(DIR + "fonts/fontSheet.png", 16, 16, new Color(103, 123, 164)), ' '));
		fonts.put("kromasky", new SpriteSheetFont(new SpriteSheet(DIR + "fonts/kromasky_16x16.png", 16, 16), ' '));
		fonts.put("kromagrad", new SpriteSheetFont(new SpriteSheet(DIR + "fonts/kromagrad_16x16.png", 16, 16), ' '));
	}
	
	private void loadSpriteSheets() throws SlickException {
		spriteSheets.put("stalker", new SpriteSheet(DIR + "images/stalker.png", 24, 28, GREEN));
		spriteSheets.put("talon", new SpriteSheet(DIR + "images/talon.png", 24, 28, GREEN));
		spriteSheets.put("carrot", new SpriteSheet(DIR + "images/carrot.png", 24, 28, GREEN));
		spriteSheets.put("silver", new SpriteSheet(DIR + "images/silver.png", 24, 28, GREEN));
		spriteSheets.put("shadow", new SpriteSheet(DIR + "images/shadow.png", 24, 28, GREEN));
		spriteSheets.put("bullets", new SpriteSheet(DIR + "images/tyrian.shp.000000.png", 12, 14, GREEN));
		spriteSheets.put("components", new SpriteSheet(DIR + "images/newsh1.shp.000000.png", 24, 28, GREEN));
		spriteSheets.put("spritesheet1", new SpriteSheet(DIR + "images/spritesheet1.png", 24, 28, GREEN));
		spriteSheets.put("spritesheet2", new SpriteSheet(DIR + "images/spritesheet2.png", 24, 28, GREEN));
		spriteSheets.put("pickups", new SpriteSheet(DIR + "images/pickups.png", 24, 28, GREEN));
	}
	
	/**
	 * @param image name of Image
	 * @return the Image accociated with the key image
	 */
	public Image getImage(String image) {
		return images.get(image);
	}
	
	/**
	 * @param animation name of Animation
	 * @return the Animation accociated with the key animation
	 */
	public Animation getAnimation(String animation) {
		MetaAnimation ma = animations.get(animation);
		Animation a = new Animation(ma.ss, ma.startX, ma.startY, ma.startX + (ma.frameCount - 1), ma.startY, true, ma.duration, true);
		if (ma.pingPong) a.setPingPong(true);
		return a;
	}
	
	/**
	 * @param sound name of Sound
	 * @return the Sound accociated with the key sound
	 */
	public Sound getSound(String sound) {
		return sounds.get(sound);
	}
	
	/**
	 * @param song name of Song
	 * @return the Music associated with the key song
	 */
	public Music getMusic(String song) {
		return music.get(song);
	}

	public SpriteSheetFont getFont(String font) {
		return fonts.get(font);
	}
	
	public SpriteSheet getSpriteSheet(String ss) {
		return spriteSheets.get(ss);
	}
	
	private class MetaAnimation {
		SpriteSheet ss;
		int startX;
		int startY;
		int frameCount;
		int duration;
		boolean pingPong = false;
		
		/**
		 * Short hand for Animation meta data where
		 * image only contains frames for that animation
		 * @param s Source spritesheet
		 * @param d Duration of animation frame
		 * @param i Number of frames
		 */
		public MetaAnimation(SpriteSheet s, int d, int i) {
			this(s, d, 0, 0, i);
		}
		
		/**
		 * Meta data for Animation based on the assumption that
		 * animations are laid out in a horizontal row of tiles
		 * @param s Source Spritesheet
		 * @param d Duration of animation frame
		 * @param x Start tile x on spritesheet
		 * @param y Start tile y on spritesheet
		 * @param i number of frames
		 */
		public MetaAnimation(SpriteSheet s, int d, int x, int y, int i) {
			ss = s;
			duration = d;
			startX = x;
			startY = y;
			frameCount = i;
		}
		
	}
}
