/**
 * 
 */
package net.aeon.epoch;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * @author Affian
 *
 */
public class ImageBackground implements Background {

	
	private Image image;
	/**
	 * @param ref
	 * @throws SlickException
	 */
	public ImageBackground(String ref) throws SlickException {
		this(new Image(ref));
	}
	
	/**
	 * @param image
	 */
	public ImageBackground(Image image) {
		this.image = image;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.Background#render(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, org.newdawn.slick.Graphics)
	 */
	@Override
	public void render(GameContainer container, PlayState game, Graphics g)
			throws SlickException {
		image.drawCentered(400, 300);

	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.Background#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(GameContainer container, PlayState game, int delta)
			throws SlickException {
		// TODO Auto-generated method stub

	}

}
