/**
 * 
 */
package net.aeon.epoch;

import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import net.aeon.epoch.Level.Event;
import net.aeon.epoch.actor.Actor;
import net.aeon.epoch.actor.Enemy;
import net.aeon.epoch.actor.GroundActor;
import net.aeon.epoch.actor.Pickup;
import net.aeon.epoch.actor.PointsPickup;
import net.aeon.epoch.actor.Actor.Team;
import net.aeon.epoch.actor.WeaponPickup;
import net.aeon.epoch.component.AttackSprite;
import net.aeon.epoch.component.BasicEnemyController;
import net.aeon.epoch.component.FlowerAttack;
import net.aeon.epoch.component.LinearAttack;
import net.aeon.epoch.component.LinearController;
import net.aeon.epoch.component.SineController;
import net.aeon.epoch.component.TargetedAttack;
import net.aeon.epoch.weapon.Laser;
import net.aeon.epoch.weapon.PlasmaCannon;

/**
 * @author Affian
 *
 */
public class ActorFactory {
	
	/** character to tokenize strings by */
	private static final String DELIM = "\\|";

	/**
	 * 
	 */
	private ActorFactory() {
		/*
		 * TODO validate params
		 * 1: create new exception to handle index out of bounds on param tokens
		 * 2: validate required elements
		 * 
		 * Exception should output the location of the actor and what element/index it
		 * broke on.
		 * ***token [index + 1] missing for param [parameter]***
		 * ***paramater [param] is required for Actor [type]***
		 */
	}
	
	/**
	 * @param event
	 * @return actor
	 */
	public static Enemy createEnemy(Event event) {
		
		Enemy actor = new Enemy(
				new Vector2f(event.x + PlayState.PLAY_MARGIN, PlayState.PLAY_MARGIN - 30), 
				180);
		
		//Required
		loadSprite(event.properties.get("sprite"), actor);
		loadController(event.properties.get("controller"), actor);
		
		//Optional
		loadValue(event.properties.get("value"), actor);
		loadHp(event.properties.get("hp"), actor);
		loadTeam(event.properties.get("team"), actor);
		loadAttack(event.properties.get("attack"), actor);
		
		actor.setHitBox(calculateHitBox(actor));
		
		return actor;
	}
	
	/**
	 * @param event
	 * @return actor
	 */
	public static GroundActor createGroundActor(Event event) {
		GroundActor actor = new GroundActor(new Vector2f(event.x + PlayState.PLAY_MARGIN, PlayState.PLAY_MARGIN), 0);
		
		//Required
		loadSprite(event.properties.get("sprite"), actor);
		
		//Optional
		loadController(event.properties.get("controller"), actor);
		loadValue(event.properties.get("value"), actor);
		loadHp(event.properties.get("hp"), actor);
		loadTeam(event.properties.get("team"), actor);
		loadAttack(event.properties.get("attack"), actor);
		
		actor.setHitBox(calculateHitBox(actor));
		
		return actor;
	}
	
	/**
	 * @param event
	 * @return actor
	 */
	public static GroundActor createDeco(Event event) {
		GroundActor actor = new GroundActor(new Vector2f(event.x + PlayState.PLAY_MARGIN, PlayState.PLAY_MARGIN), 0);
		loadSprite(event.properties.get("sprite"), actor);
		actor.setTeam(Team.NEUTRAL);
		return actor;
	}
	
	/**
	 * @param event
	 * @return actor
	 */
	public static Pickup createPickup(Event event) {
		String type = event.type;
		Pickup actor = null;
		if (type != null) {
			if (type.equalsIgnoreCase("points")) {
				actor = new PointsPickup(new Vector2f(event.x + PlayState.PLAY_MARGIN, PlayState.PLAY_MARGIN));
			} else if (type.equalsIgnoreCase("weapon")) {
				actor = new WeaponPickup(new Vector2f(event.x + PlayState.PLAY_MARGIN, PlayState.PLAY_MARGIN));
				loadWeapon(event.properties.get("weapon"), actor);
			}
		}
		if (actor != null) {
			loadValue(event.properties.get("value"), actor);
			actor.setTeam(Team.NEUTRAL);
		}
		return actor;
	}
	
	/**
	 * @param param
	 * @param actor
	 */
	private static void loadTeam(String param, Actor actor) {
		if (param != null) {
			if (param.equalsIgnoreCase("enemy")) {
				actor.setTeam(Team.ENEMY);
			} else if (param.equalsIgnoreCase("neutral")) {
				actor.setTeam(Team.NEUTRAL);
			} else if (param.equalsIgnoreCase("player")) {
				actor.setTeam(Team.PLAYER);
			}
		}
	}

	/**
	 * @param param
	 * @param actor
	 */
	private static void loadValue(String param, Actor actor) {
		if (param != null) {
			actor.setValue(Integer.parseInt(param));
		} else {
			actor.setValue(10);
		}
	}
	
	/**
	 * @param param
	 * @param actor
	 */
	private static void loadHp(String param, Actor actor) {
		if (param != null) {
			actor.setHp(Integer.parseInt(param));
		} else {
			actor.setHp(10);
		}
	}

	/**
	 * @param param
	 * @param actor
	 */
	private static void loadController(String param, Actor actor) {
		String[] tokens = null;
		
		if (param != null) {
			tokens = param.split(DELIM);
			if (tokens[0].equalsIgnoreCase("linear")) {
				actor.setController(new LinearController(actor, Float.parseFloat(tokens[1]), Float.parseFloat(tokens[2])));
			} else if (tokens[0].equalsIgnoreCase("sine")) {
				actor.setController(new SineController(actor, Float.parseFloat(tokens[1])));
			} else if (tokens[0].equalsIgnoreCase("basicEnemy")) {
				actor.setController(new BasicEnemyController(actor, Float.parseFloat(tokens[1]), Float.parseFloat(tokens[2])));
			}
		}
	}

	/**
	 * @param param
	 * @param actor
	 */
	private static void loadSprite(String param, Actor actor) {
		String[] tempTokens = null;
		
		if (param != null) {
			tempTokens = param.split(DELIM);
			if (tempTokens[0].equalsIgnoreCase("image")) {
				actor.setSprite(ResourceManager.getInstance().getImage(tempTokens[1]));
			} else if (tempTokens[0].equalsIgnoreCase("animation")){
				actor.setSprite(ResourceManager.getInstance().getAnimation(tempTokens[1]));
			} else if (tempTokens[0].equalsIgnoreCase("attack")) {
				actor.setSprite(new AttackSprite(actor, ResourceManager.getInstance().getAnimation(tempTokens[1])));
			}
			actor.getPosition().y = PlayState.PLAY_MARGIN - actor.getSprite().getHeight();
		}
	}
	
	/**
	 * @param param
	 * @param actor
	 */
	private static void loadAttack(String param, Actor actor) {
		String[] tempTokens = null;
		
		if (param != null) {
			tempTokens = param.split(DELIM);
			if (tempTokens[0].equalsIgnoreCase("target")) {
				if (tempTokens.length == 2)
					actor.setAttack(new TargetedAttack(actor, Integer.parseInt(tempTokens[1]), "bullet2"));
				else if (tempTokens.length == 4)
					actor.setAttack(new TargetedAttack(actor, Integer.parseInt(tempTokens[1]), Integer.parseInt(tempTokens[2]), Integer.parseInt(tempTokens[3]), "bullet2"));
			} else if (tempTokens[0].equalsIgnoreCase("flower")){
				//TODO parse flower parameters
				actor.setAttack(new FlowerAttack(actor));
			} else if (tempTokens[0].equalsIgnoreCase("linear")) {
				if (tempTokens.length == 3)
					actor.setAttack(new LinearAttack(actor, Integer.parseInt(tempTokens[1]), Float.parseFloat(tempTokens[2]), "bullet2"));
				else
					actor.setAttack(new LinearAttack(actor, Integer.parseInt(tempTokens[1]), Integer.parseInt(tempTokens[2]), Integer.parseInt(tempTokens[3]), Float.parseFloat(tempTokens[4]), "bullet2"));
			}
		}
	}
	
	/**
	 * @param param
	 * @param actor
	 */
	private static void loadWeapon(String param, Actor actor) {
		String[] tempTokens = null;
		
		if (param != null) {
			tempTokens = param.split(DELIM);
			if (tempTokens[0].equalsIgnoreCase("plasma")) {
				((WeaponPickup)actor).setWeapon(new PlasmaCannon());
			} else if (tempTokens[0].equalsIgnoreCase("laser")) {
				((WeaponPickup)actor).setWeapon(new Laser());
			}
		}
	}
	
	/**
	 * @param actor
	 * @return {@link org.newdawn.slick.geom.Rectangle Rectangle} defines actors hitbox
	 */
	private static Rectangle calculateHitBox(Actor actor) {
		if (actor.getSprite() != null)
			return new Rectangle(0,0,Math.max(actor.getSprite().getWidth() - 12, 24),actor.getSprite().getHeight() - 16);
		return null;
	}
	
}
