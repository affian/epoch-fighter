/**
 * 
 */
package net.aeon.epoch;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

/**
 * @author Affian
 *
 */
public interface Background {
	
	public void render(GameContainer container, PlayState game, Graphics g) throws SlickException;
	public void update(GameContainer container, PlayState game, int delta) throws SlickException;
}
