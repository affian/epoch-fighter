/**
 * 
 */
package net.aeon.epoch;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

/**
 * This Starfield as a Background is costing about 200 FPS in game
 * the updating of the stars doubles the number of entities to update
 * @author Affian
 */
public class StarField implements Background {
	
	private static final float LAYER1_SPEED = 0.3f;
	private static final float LAYER2_SPEED = 0.4f;
	private static final float LAYER3_SPEED = 0.55f;
	private static final float STAR_RATE = 0.99f;
	
	private ArrayList<Star> starsLayer1;
	private ArrayList<Star> starsLayer2;
	private ArrayList<Star> starsLayer3;
	
	private Image star1Sprite;
	private Image star2Sprite;
	private Image star3Sprite;
	private Image star4Sprite;

	/**
	 * 
	 */
	public StarField() {

		ResourceManager rm = ResourceManager.getInstance();
		star1Sprite = rm.getImage("star1");
		star2Sprite = rm.getImage("star2");
		star3Sprite = rm.getImage("star3");
		star4Sprite = rm.getImage("star4");
		
		starsLayer1 = new ArrayList<Star>();
		starsLayer2 = new ArrayList<Star>();
		starsLayer3 = new ArrayList<Star>();
	}
	
	/**
	 * 
	 * @param container
	 * @param game
	 * @param g
	 * @throws SlickException
	 */
	public void render(GameContainer container, PlayState game, Graphics g)
			throws SlickException {
		
		for (Star star:starsLayer1) {
			star.sprite.draw(star.pos.x, star.pos.y, 0.5f);
		}
		for (Star star:starsLayer2) {
			star.sprite.draw(star.pos.x, star.pos.y, 0.5f);
		}
		for (Star star:starsLayer3) {
			star.sprite.draw(star.pos.x, star.pos.y, 0.5f);
		}
		
	}

	/**
	 * 
	 * @param container
	 * @param game
	 * @param delta
	 * @throws SlickException
	 */
	public void update(GameContainer container, PlayState game, int delta)
			throws SlickException {
		ArrayList<Star> delete = new ArrayList<Star>();
		
		/* TODO this random code is dependent on the games FPS,
		 * higher FPS makes more stars and more stars lower FPS.
		 */
		if (Math.random() > STAR_RATE) 
			starsLayer1.add(new Star(Math.random() >= 0.5 ? star1Sprite : star2Sprite, new Vector2f((float)((Math.random() * 400) + 196), 0 - star3Sprite.getHeight())));
		if (Math.random() > STAR_RATE) 
			starsLayer2.add(new Star(Math.random() >= 0.5 ? star2Sprite : star3Sprite, new Vector2f((float)((Math.random() * 400) + 196), 0 - star3Sprite.getHeight())));
		if (Math.random() > STAR_RATE) 
			starsLayer3.add(new Star(Math.random() >= 0.5 ? star3Sprite : star4Sprite, new Vector2f((float)((Math.random() * 400) + 196), 0 - star3Sprite.getHeight())));
		
		for (Star star:starsLayer1) {
			star.pos.y += LAYER1_SPEED * delta;
			if (star.pos.y > 600) delete.add(star);
		}
		for (Star star:starsLayer2) {
			star.pos.y += LAYER2_SPEED * delta;
			if (star.pos.y > 600) delete.add(star);
		}
		for (Star star:starsLayer3) {
			star.pos.y += LAYER3_SPEED * delta;
			if (star.pos.y > 600) delete.add(star);
		}
		starsLayer1.removeAll(delete);
		starsLayer2.removeAll(delete);
		starsLayer3.removeAll(delete);
	}
	
	private class Star {
		public Vector2f pos;
		public Image sprite;
		
		public Star(Image sprite, Vector2f pos) {
			this.sprite = sprite;
			this.pos = pos;
		}
	}

}
