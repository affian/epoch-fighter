/**
 * 
 */
package net.aeon.epoch.component;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.actor.Actor;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

/**
 * @author Affian
 *
 */
public class ImageSprite implements Sprite {
	
	private Actor owner;
	private Image image;

	/**
	 * 
	 */
	public ImageSprite(Actor owner, Image image) {
		this.owner = owner;
		this.image = image;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.controllers.Sprite#getHeight()
	 */
	@Override
	public int getHeight() {
		return image.getHeight();
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.controllers.Sprite#getWidth()
	 */
	@Override
	public int getWidth() {
		return image.getWidth();
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.controllers.Sprite#render(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, org.newdawn.slick.Graphics)
	 */
	@Override
	public void render(GameContainer container, PlayState game, Graphics g)
			throws SlickException {
		//FIXME replace rotation support
		//image.setRotation(owner.getHeading());
		if (owner.isHit()) {
			if (owner.isCentered())
				image.drawFlash((int)(owner.getAbsoluteX(game)-(image.getWidth()/2)),(int)(owner.getAbsoluteY(game)-(image.getHeight()/2)));
			else
				image.drawFlash((int)owner.getAbsoluteX(game),(int)owner.getAbsoluteY(game));
		} else {
			if (owner.isCentered())
				image.draw((int)(owner.getAbsoluteX(game)-(image.getWidth()/2)),(int)(owner.getAbsoluteY(game)-(image.getHeight()/2)));
			else
				image.draw((int)owner.getAbsoluteX(game),(int)owner.getAbsoluteY(game));
		}

	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.controllers.Sprite#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(GameContainer container, PlayState game, int delta)
			throws SlickException {
		//Nothing
	}

	@Override
	public Image getImage() {
		return image;
	}

	@Override
	public Vector2f getCenter() {
		if (owner.isCentered()) {
			return new Vector2f(owner.getPosition());
		} else {
			float x = owner.getPosition().x + image.getWidth() / 2;
			float y = owner.getPosition().y + image.getWidth() / 2;
			return new Vector2f(x, y);
		}
	}

}
