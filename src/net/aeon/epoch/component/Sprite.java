/**
 * 
 */
package net.aeon.epoch.component;

import net.aeon.epoch.PlayState;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

/**
 * @author Affian
 *
 */
public interface Sprite {
	public void update(GameContainer container, PlayState game, int delta) throws SlickException;
	public void render(GameContainer container, PlayState game, Graphics g) throws SlickException;
	public int getWidth();
	public int getHeight();
	public Image getImage();
	public Vector2f getCenter();
	
	
}
