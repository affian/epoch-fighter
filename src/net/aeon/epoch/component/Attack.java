/**
 * 
 */
package net.aeon.epoch.component;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.actor.Actor;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;

/**
 * @author Affian
 *
 */
public interface Attack {
	public void update(GameContainer container, PlayState game, int delta) throws SlickException;
	public void setOwner(Actor owner);
	public int getTimer();
	public Actor getOwner();
}
