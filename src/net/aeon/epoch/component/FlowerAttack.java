/**
 * 
 */
package net.aeon.epoch.component;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.actor.Actor;
import net.aeon.epoch.actor.Bullet;
import net.aeon.epoch.actor.Actor.Team;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

/**
 * @author Affian
 *
 */
public class FlowerAttack implements Attack {

	private Actor owner;
	private int burstSize;
	private int attackDelay;
	private String bullet;
	private boolean starBurst;
	private int burstRate;
	
	private int timer;
	private int burstCount;
	private int burstDelay;
	
	/**
	 * @param owner
	 */
	public FlowerAttack(Actor owner) {
		this(owner, 20, 1000, 20, 1000, "bullet2", false);
	}
	
	/**
	 * attack: <burstsize>|<attackdelay>|<burstRate>|<initialdelay>
	 * 
	 * @param owner
	 * @param burstSize
	 * @param attackDelay
	 * @param burstRate
	 * @param initialDelay
	 * @param bullet
	 * @param starBurst
	 */
	public FlowerAttack(Actor owner, int burstSize, int attackDelay, int burstRate, int initialDelay, String bullet, boolean starBurst) {
		this.owner = owner;
		this.burstSize = burstSize;
		this.attackDelay = attackDelay;
		this.bullet = bullet;
		this.starBurst = starBurst;
		this.burstRate = burstRate;
		
		this.timer = initialDelay;
		this.burstCount = 0;
		this.burstDelay = 0;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Attack#getOwner()
	 */
	@Override
	public Actor getOwner() {
		return owner;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Attack#setOwner(net.aeon.epoch.actor.Actor)
	 */
	@Override
	public void setOwner(Actor owner) {
		this.owner = owner;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Attack#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(GameContainer container, PlayState game, int delta)
			throws SlickException {
		if (timer <= 0) {
			if (burstDelay <= 0) {
				attack(game, delta);
			} else {
				burstDelay -= delta;
			}
		} else {
			timer -= delta;
		}

	}
	
	/**
	 * @param game
	 * @param delta
	 */
	private void attack(PlayState game, int delta) {
		Bullet bullet = null;
		
		if (starBurst) {
			while (burstCount < burstSize) {
				bullet = new Bullet();
				bullet.setTeam(Team.ENEMY);
				bullet.setSprite(game.getResourceManager().getImage(this.bullet));
				bullet.setHitBox(new Rectangle(0,0,5,5));
				bullet.setPosition(owner.getSprite().getCenter());
				bullet.setController(new LinearController(bullet, 0.3f, 360 / burstSize * burstCount));
				game.addActor(bullet);
				
				burstCount++;
			}
		} else {
			bullet = new Bullet();
			bullet.setTeam(Team.ENEMY);
			bullet.setSprite(game.getResourceManager().getImage(this.bullet));
			bullet.setHitBox(new Rectangle(0,0,5,5));
			bullet.setPosition(owner.getSprite().getCenter());
			bullet.setController(new LinearController(bullet, 0.3f, 360 / burstSize * burstCount));
			game.addActor(bullet);
			
			burstCount++;
		}
		

		if (burstCount < burstSize) {
			burstDelay = burstRate;
			
		} else {
			timer = attackDelay;
			burstCount = 0;
		}
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Attack#getTimer()
	 */
	@Override
	public int getTimer() {
		return timer;
	}

}
