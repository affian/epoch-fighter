/**
 * 
 */
package net.aeon.epoch.component;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.actor.Actor;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

/**
 * @author Affian
 *
 */
public class LinearController extends Controller {
	
	private float speed;
	private float heading;
	private boolean follow;

	/**
	 * @param owner
	 */
	public LinearController(Actor owner, float speed, float heading) {
		super(owner);
		this.setSpeed(speed);
		this.setFollow(true);
		this.setHeading(heading);
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.controllers.Controller#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(GameContainer container, PlayState game, int delta)
			throws SlickException {
		Vector2f pos = this.getOwner().getPosition();
		
		float hip = speed * delta;
        pos.x += (hip * java.lang.Math.sin(java.lang.Math.toRadians(heading)));
        pos.y -= (hip * java.lang.Math.cos(java.lang.Math.toRadians(heading)));
        
        //FIXME massive clutch
        //if (heading == 90 || heading == 270) pos.y += game.getLevel().getSpeed() * delta;

	}

	/**
	 * @return the speed
	 */
	public float getSpeed() {
		return speed;
	}

	/**
	 * @return the heading
	 */
	public float getHeading() {
		return heading;
	}

	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(float speed) {
		this.speed = speed;
	}

	/**
	 * @param heading the heading to set
	 */
	public void setHeading(float heading) {
		if (follow)
			getOwner().setHeading(heading);
		this.heading = heading;
	}

	/**
	 * @param follow the follow to set
	 */
	public void setFollow(boolean follow) {
		this.follow = follow;
	}

	/**
	 * @return the follow
	 */
	public boolean isFollow() {
		return follow;
	}

}
