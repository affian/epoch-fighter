/**
 * 
 */
package net.aeon.epoch.component;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.actor.Actor;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

/**
 * @author Affian
 *
 */
public class AnimatedSprite implements Sprite {
	
	protected Actor owner;
	protected Animation animation;

	/**
	 * 
	 */
	public AnimatedSprite(Actor owner, Animation animation) {
		this.animation = animation;
		this.owner = owner;
		
		this.animation.setAutoUpdate(false);
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Sprite#getHeight()
	 */
	@Override
	public int getHeight() {
		return animation.getCurrentFrame().getHeight();
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Sprite#getImage()
	 */
	@Override
	public Image getImage() {
		return animation.getCurrentFrame();
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Sprite#getWidth()
	 */
	@Override
	public int getWidth() {
		return animation.getCurrentFrame().getWidth();
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Sprite#render(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, org.newdawn.slick.Graphics)
	 */
	@Override
	public void render(GameContainer container, PlayState game, Graphics g)
			throws SlickException {
		if (owner.isHit()) {
			if (owner.isCentered())
				animation.drawFlash((int)(owner.getAbsoluteX(game)-(getWidth()/2)),(int)(owner.getAbsoluteY(game)-(getHeight()/2)), getWidth(), getHeight());
			else
				animation.drawFlash((int)owner.getAbsoluteX(game),(int)owner.getAbsoluteY(game), getWidth(), getHeight());
		} else {
			if (owner.isCentered())
				animation.draw((int)(owner.getAbsoluteX(game)-(getWidth()/2)),(int)(owner.getAbsoluteY(game)-(getHeight()/2)));
			else
				animation.draw((int)owner.getAbsoluteX(game),(int)owner.getAbsoluteY(game));
		}

	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Sprite#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(GameContainer container, PlayState game, int delta)
			throws SlickException {
		animation.update(delta);

	}
	
	public Vector2f getCenter() {
		if (owner.isCentered()) {
			return new Vector2f(owner.getPosition());
		} else {
			float x = owner.getPosition().x + getImage().getWidth() / 2;
			float y = owner.getPosition().y + getImage().getWidth() / 2;
			return new Vector2f(x, y);
		}
	}

}
