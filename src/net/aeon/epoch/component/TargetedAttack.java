/**
 * 
 */
package net.aeon.epoch.component;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.actor.Actor;
import net.aeon.epoch.actor.Bullet;
import net.aeon.epoch.actor.Actor.Team;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

/**
 * @author Affian
 *
 */
public class TargetedAttack implements Attack {
	
	private Actor owner;
	private int burstSize;
	private int attackDelay;
	private String bullet;
	
	private int timer;
	private int burstCount;
	private int burstDelay;
	
	/**
	 * @param owner
	 */
	public TargetedAttack(Actor owner) {
		this(owner, 1, 700, 700, "bullet2");
	}
	
	/**
	 * @param owner
	 * @param attackDelay
	 * @param bullet
	 */
	public TargetedAttack(Actor owner, int attackDelay, String bullet) {
		this(owner, 1, attackDelay, attackDelay, bullet);
	}
	
	/**
	 * 
	 * @param owner
	 * @param burstSize
	 * @param attackDelay
	 * @param initialDelay
	 * @param bullet
	 */
	public TargetedAttack(Actor owner, int burstSize, int attackDelay, int initialDelay, String bullet) {
		this.owner = owner;
		this.burstSize = burstSize;
		this.attackDelay = attackDelay;
		this.bullet = bullet;
		this.timer = initialDelay;
		this.burstDelay = 0;
		this.burstCount = 0;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Attack#getOwner()
	 */
	@Override
	public Actor getOwner() {
		return owner;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Attack#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(GameContainer container, PlayState game, int delta)
			throws SlickException {
		if (timer <= 0) {
			if (burstDelay <= 0) {
				attack(game, delta);
			} else {
				burstDelay -= delta;
			}
		} else {
			timer -= delta;
		}
	
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Attack#setOwner(net.aeon.epoch.actor.Actor)
	 */
	@Override
	public void setOwner(Actor owner) {
		this.owner = owner;
	}
	
	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Attack#getTimer()
	 */
	@Override
	public int getTimer() {
		return timer;
	}

	private void attack(PlayState game, int delta) {
		Bullet bullet = new Bullet();
		float a = owner.getPosition().x - game.getPlayer().getPosition().x;
		float b = owner.getPosition().y - game.getPlayer().getPosition().y;
		
		bullet.setTeam(Team.ENEMY);
		bullet.setSprite(game.getResourceManager().getImage(this.bullet));
		bullet.setHitBox(new Rectangle(0,0,5,5));
		bullet.setPosition(owner.getSprite().getCenter());
		bullet.setController(new LinearController(bullet, 0.3f, (float)Math.toDegrees(Math.atan2(-a, b))));
		game.addActor(bullet);
		
		burstCount++;
		
		if (burstCount < burstSize) {
			burstDelay = 50; //milliseconds
			
		} else {
			timer = attackDelay;
			burstCount = 0;
		}
	}

}
