/**
 * 
 */
package net.aeon.epoch.component;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.actor.Actor;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;

/**
 * @author Affian
 *
 */
public class AttackSprite extends AnimatedSprite {

	/**
	 * @param owner
	 * @param animation
	 */
	public AttackSprite(Actor owner, Animation animation) {
		super(owner, animation);
		animation.setPingPong(true);
		animation.stop();
	}
	
	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.AnimatedSprite#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(GameContainer container, PlayState game, int delta)
		throws SlickException {
		if (animation.getFrame() == animation.getFrameCount() - 1)
			animation.stopAt(0);
		
		super.update(container, game, delta);
		
		if (owner.getAttack().getTimer() - getAttackDelay() <= 0)
			startAttack();
	}
	
	/**
	 * Starts the attack animation
	 */
	public void startAttack() {
		animation.stopAt(animation.getFrameCount() - 1);
		animation.start();
		
	}
	
	/**
	 * Get the number of milliseconds that it takes for the attack 
	 * animation to play, before the bullet should be created.
	 * @return number of milliseconds from the start of the first frame to the start of the last frame
	 */
	public int getAttackDelay() {
		int time = 0;
		for (int i = 0; i < animation.getFrameCount() - 1; i++) {
			time += animation.getDuration(i); //combine all frame durations except for the last frame
		}
		return time;
	}

}
