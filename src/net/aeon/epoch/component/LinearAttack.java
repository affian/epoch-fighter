/**
 * 
 */
package net.aeon.epoch.component;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.actor.Actor;
import net.aeon.epoch.actor.Bullet;
import net.aeon.epoch.actor.Actor.Team;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

/**
 * @author Affian
 *
 */
public class LinearAttack implements Attack {

	private Actor owner;
	private int burstSize;
	private int attackDelay;
	private float heading;
	private String bullet;
	
	private int timer;
	private int burstCount;
	private int burstDelay;
	
	/**
	 * Creates a single shot linear attack
	 * 
	 * @param owner Actor that has this attack
	 * @param attackDelay Time in milliseconds between attacks
	 * @param heading Direction of attack in degrees from north
	 * @param bullet String reference to the bullet to be fired
	 */
	public LinearAttack(Actor owner, int attackDelay, float heading, String bullet) {
		this(owner, 1, attackDelay, attackDelay, heading, bullet);
	}
	
	/**
	 * Creates a linear attack capable of burst fire
	 * 
	 * @param owner Actor that has this attack
	 * @param burstSize Number of shots to be fired per attack
	 * @param attackDelay Time in milliseconds between attacks
	 * @param initialDelay Time in milliseconds before first attack
	 * @param heading Direction of attack in degrees from north
	 * @param bullet String reference to the bullet to be fired
	 */
	public LinearAttack(Actor owner, int burstSize, int attackDelay, int initialDelay, float heading, String bullet) {
		this.setOwner(owner);
		this.burstSize = burstSize;
		this.attackDelay = attackDelay;
		this.timer = initialDelay;
		this.burstDelay = 0;
		this.burstCount = 0;
		this.bullet = bullet;
		this.heading = heading;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Attack#getOwner()
	 */
	@Override
	public Actor getOwner() {
		return owner;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Attack#getTimer()
	 */
	@Override
	public int getTimer() {
		return timer;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Attack#setOwner(net.aeon.epoch.actor.Actor)
	 */
	@Override
	public void setOwner(Actor owner) {
		this.owner = owner;

	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Attack#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(GameContainer container, PlayState game, int delta)
			throws SlickException {
		if (timer <= 0) {
			if (burstDelay <= 0) {
				attack(game, delta);
			} else {
				burstDelay -= delta;
			}
		} else {
			timer -= delta;
		}

	}

	private void attack(PlayState game, int delta) {
		
		Bullet bullet = new Bullet();
		bullet.setTeam(Team.ENEMY);
		bullet.setSprite(game.getResourceManager().getImage(this.bullet));
		bullet.setHitBox(new Rectangle(0,0,5,5));
		bullet.setPosition(owner.getSprite().getCenter());
		bullet.setController(new LinearController(bullet, 0.3f, heading));
		game.addActor(bullet);
		
		burstCount++;
		
		if (burstCount < burstSize) {
			burstDelay = 50; //milliseconds
			
		} else {
			timer = attackDelay;
			burstCount = 0;
		}
		
	}

}
