/**
 * 
 */
package net.aeon.epoch.component;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.actor.Actor;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;

/**
 * TODO Delete me
 * @author Affian
 *
 */
public class BasicEnemyController extends LinearController {

	private int rof;
	private int attackTimer;
	private boolean loop;
	/**
	 * @param owner
	 */
	public BasicEnemyController(Actor owner, float heading, float speed) {
		super(owner, heading, speed);
		this.rof = 1000;
		this.attackTimer = 1000;
		this.loop = true;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.controllers.Controller#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(GameContainer container, PlayState game, int delta)
			throws SlickException {
		super.update(container, game, delta);
		
		if (attackTimer > 0) attackTimer -= delta;
		if (attackTimer <= 0) {
			//getOwner().attack(game, delta);
			if (loop) attackTimer = rof;
		}

	}

}
