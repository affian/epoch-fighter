/**
 * 
 */
package net.aeon.epoch.component;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.actor.Actor;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Vector2f;

/**
 * @author Affian
 *
 */
public class BankingSprite implements Sprite {
	
	/** Number of milliseconds between banking advancements */
	private static final int BANK_STEP = 100;

	private Actor owner;
	private SpriteSheet sprites;
	private int bankDelta;
	private float lastX;
	private int currentSprite;
	
	/**
	 * 
	 */
	public BankingSprite(Actor owner, SpriteSheet sprites) {
		this.owner = owner;
		this.sprites = sprites;
		this.bankDelta = 0;
		this.lastX = owner.getPosition().x;
		this.currentSprite = sprites.getHorizontalCount() / 2 + 1; //the middle sprite
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Sprite#getHeight()
	 */
	@Override
	public int getHeight() {
		return sprites.getSprite(currentSprite, 0).getHeight();
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Sprite#getImage()
	 */
	@Override
	public Image getImage() {
		return sprites.getSprite(currentSprite, 0);
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Sprite#getWidth()
	 */
	@Override
	public int getWidth() {
		return sprites.getSprite(currentSprite, 0).getWidth();
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Sprite#render(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, org.newdawn.slick.Graphics)
	 */
	@Override
	public void render(GameContainer container, PlayState game, Graphics g)
			throws SlickException {
		if (owner.isHit()) {
			getCurrentSprite().drawFlash((int)(owner.getAbsoluteX(game)-(getCurrentSprite().getWidth()/2)),(int)(owner.getAbsoluteY(game)-(getCurrentSprite().getHeight()/2)));
		} else {
			getCurrentSprite().draw((int)(owner.getAbsoluteX(game)-(getCurrentSprite().getWidth()/2)),(int)(owner.getAbsoluteY(game)-(getCurrentSprite().getHeight()/2)));
		}

	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.component.Sprite#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(GameContainer container, PlayState game, int delta)
			throws SlickException {
		if (owner.getPosition().x < lastX) {//moving left
			bankDelta -= delta;
		} else if (owner.getPosition().x > lastX) {//moving right
			bankDelta += delta;
		} else {
			//TODO move bankDelta back to 0 based on delta
			bankDelta = 0;
		}
		
		//TODO a better solution for banking animations is needed
		if (bankDelta < -(BANK_STEP * 2)) {
			currentSprite = 0;
		}else if (bankDelta < -(BANK_STEP)) {
			currentSprite = 1;
		} else if (bankDelta > BANK_STEP * 2) {
			currentSprite = 4;
		} else if (bankDelta > BANK_STEP) {
			currentSprite = 3;
		} else {
			currentSprite = 2;
		}
			
		lastX = owner.getPosition().x;
	}
	
	public Image getCurrentSprite() {
		return sprites.getSprite(currentSprite, 0);
	}
	
	@Override
	public Vector2f getCenter() {
		if (owner.isCentered()) {
			return new Vector2f(owner.getPosition());
		} else {
			float x = owner.getPosition().x + getCurrentSprite().getWidth() / 2;
			float y = owner.getPosition().y + getCurrentSprite().getWidth() / 2;
			return new Vector2f(x, y);
		}
	}

}
