/**
 * 
 */
package net.aeon.epoch.component;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.actor.Player;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

/**
 * @author Affian
 *
 */
public class KeyboardController extends Controller {

	/**
	 * @param owner
	 */
	public KeyboardController(Player owner) {
		super(owner);
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.controllers.Controller#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(GameContainer container, PlayState game, int delta)
			throws SlickException {
		Input input = container.getInput();
		
		Vector2f position = getOwner().getPosition();
		
		if (input.isKeyDown(Input.KEY_SPACE) /*|| input.isButtonPressed(0, Input.ANY_CONTROLLER)*/){
			Player player = (Player) getOwner();
			player.attack(game, delta);
		}
		if (input.isKeyDown(Input.KEY_UP) || input.isKeyDown(Input.KEY_W) /*|| input.isControllerUp(Input.ANY_CONTROLLER)*/){
			position.y -= 0.3f * delta;
		}
		if (input.isKeyDown(Input.KEY_DOWN) || input.isKeyDown(Input.KEY_S) /*|| input.isControllerDown(Input.ANY_CONTROLLER)*/){
			position.y += 0.3f * delta;
		}
		if (input.isKeyDown(Input.KEY_LEFT) || input.isKeyDown(Input.KEY_A) /*|| input.isControllerLeft(Input.ANY_CONTROLLER)*/){
			position.x -= 0.3f * delta;
		}
		if (input.isKeyDown(Input.KEY_RIGHT) || input.isKeyDown(Input.KEY_D) /*|| input.isControllerRight(Input.ANY_CONTROLLER)*/){
			position.x += 0.3f * delta;
		}

	}

}
