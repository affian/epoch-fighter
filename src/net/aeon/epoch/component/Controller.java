/**
 * 
 */
package net.aeon.epoch.component;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.actor.Actor;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;

/**
 * @author Affian
 *
 */
public abstract class Controller {
	
	private Actor owner;
	

	/**
	 * 
	 */
	public Controller(Actor owner) {
		this.setOwner(owner);
	}
	
	/**
	 * 
	 * @param container
	 * @param game
	 * @param delta
	 * @throws SlickException
	 */
	public abstract void update(GameContainer container, PlayState game, int delta)
			throws SlickException;

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(Actor owner) {
		this.owner = owner;
	}

	/**
	 * @return the owner
	 */
	public Actor getOwner() {
		return owner;
	}

}
