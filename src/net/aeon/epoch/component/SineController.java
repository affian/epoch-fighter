/**
 * 
 */
package net.aeon.epoch.component;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.actor.Actor;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

/**
 * Sine Controller make the actor oscillate along the X axis according to
 * the formula "X = 0.25f * Math.sin(0.03f * Y)".
 * This controller will make the actor move down the screen only according to
 * the set speed.
 * @author Affian
 */
public class SineController extends Controller {

	private float speed;
	
	/**
	 * @param owner
	 * @param speed
	 */
	public SineController(Actor owner, float speed) {
		super(owner);
		this.speed = speed;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.controllers.Controller#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(GameContainer container, PlayState game, int delta)
			throws SlickException {
		
		Vector2f pos = this.getOwner().getPosition();
		float distance = speed * delta;
		
		pos.y += distance;
		pos.x += 0.25f * Math.sin(0.03f * pos.y);

	}

}
