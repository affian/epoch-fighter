/**
 * 
 */
package net.aeon.epoch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import net.aeon.epoch.Cutscene.Side;
import net.aeon.epoch.actor.Actor;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

/**
 * w18xh22 tiles to fill the play area with the current sidebars.
 * Tested up to width of 32 tiles
 * @author Affian
 */
public class Level extends TiledMap {
	
	//Static data
	/** Number of vertical tiles it takes to fill the screen */
	private int height;
	/** Does the map loop? */
	private boolean looped;
	/** Name of the Level */
	private String name;
	/** Background to render behind the level */
	private Background background;
	
	//Transient data
	/** The y coordinate based on screen location */
	private float y;
	/** The Y index for where we are on the map in tiles */
	private int sy;
	/** The scroll speed of the map */
	private float speed;
	/** List of loaded Cutscenes indexed by Cutscene reference name */
	private HashMap<String,Cutscene> cutscenes;
	/** List of events in the TilED map indexed by the map row they occur on */
	private HashMap<Integer, LinkedList<Event>> events;
	
	/**
	 * @param ref file reference for the level's file location
	 * @throws SlickException 
	 */
	public Level(String ref) throws SlickException {
		super(ref);
		this.cutscenes = new HashMap<String, Cutscene>();
		this.events = new HashMap<Integer, LinkedList<Event>>();
		
		//Map properties
		this.name = this.getMapProperty("name", "Undefined");
		this.speed = Float.parseFloat(this.getMapProperty("speed", "0.1"));
		this.looped = Boolean.parseBoolean(this.getMapProperty("loop", "false"));
		
		//Map Coordinates
		this.height = 600 / this.getTileHeight() + 2;//+2 extends map 1 tile beyond the top and bottom of the screen
		this.y = -(this.getTileHeight());
		this.sy = this.getHeight() - height;
		
		parseBackground();
		parseEvents();

	}
	
	private void parseBackground() throws SlickException {
		String temp = this.getMapProperty("background", "");
		if (!temp.equals("")) {
			String tokens[] = temp.split("\\|");
			if (tokens[0].equalsIgnoreCase("image")) {
				this.background = new ImageBackground("/resources/images/" + tokens[1]);
			} else if (tokens[0].equalsIgnoreCase("starfield")) {
				this.background = new StarField();
			}
		}
	}

	/**
	 *  Parses the Objects in the TiLED map into Event objects and stores
	 *  them for later use, indexed by their location on the map.
	 */
	@SuppressWarnings("unchecked")
	private void parseEvents() {
		for (int i = 0; i < this.getObjectGroupCount(); i++) {
			ObjectGroup og = (ObjectGroup) objectGroups.get(i);
			int j = 0;
			for (GroupObject event:(ArrayList<GroupObject>)og.objects) {
				Event e = new Event(j++, i, 
						event.y,
						event.x,
						event.name,
						event.type);
				if (event.props != null) 
					for (String key:event.props.stringPropertyNames())
						e.properties.put(key, event.props.getProperty(key));
				if (e.type.equals("cutscene")) {
					parseCutscene(e);
				}
				LinkedList<Event> list = events.get((int)event.y/this.getTileHeight());
				if (list == null) {
					list = new LinkedList<Event>();
					events.put((int)event.y/this.getTileHeight(), list);
				}
				list.add(e);
			}
		}
	}
	
	/**
	 * Pre-loads a cutscene contained within an event in this level
	 * ===Event config===
	 * Name: cutscene reference name
	 * Type: "cutscene"
	 * Properties:
	 * 		dialog-1: speaker|left/right|text
	 *		dialog-'n': speaker|left/right|text
	 * 
	 * @param e Event containing a Cutscene
	 */
	private void parseCutscene(Event e) {
		ResourceManager rm = ResourceManager.getInstance();
		String[] tokens = null;
		
		Cutscene c = new Cutscene(e.name, rm.getImage("textBackdrop"));
		for (Entry<String, String> entry:e.properties.entrySet()) {
			if (entry.getKey().startsWith("dialog")) {
				tokens = entry.getValue().split("\\|");
				if (tokens.length == 3) {
					c.addSpeaker(tokens[0], rm.getImage(tokens[0]));
					c.addDialog(tokens[0], tokens[2], (tokens[1].equals("left"))?Side.LEFT:Side.RIGHT);
				} else {
					System.err.println("Invalid dialog syntax@" + e.name + ": " + entry.getKey() + "-" + entry.getValue());
				}
			}
		}
		cutscenes.put(c.getName(), c);
	}

	/**
	 * Render lowest tile level before any actors or particles are
	 * drawn, also renders any background animations.
	 * @param container GameContainer
	 * @param game PlayState that is the parent of this level
	 * @param g access to Graphics canvas
	 * @throws SlickException
	 */
	public void preRender(GameContainer container, PlayState game, Graphics g)
		throws SlickException {
		if (background != null)
			background.render(container, game, g);
		this.render((int)game.getPlayArea().getX() + PlayState.PLAY_MARGIN, (int)y, 0, sy, this.getWidth(), height);
	}
	
	/**
	 * Render map tile layers over actors and particles, ideal for clouds
	 * and other obstructions.
	 * @param container GameContainer
	 * @param game PlayState that is the parent of this level
	 * @param g access to Graphics canvas
	 * @throws SlickException
	 */
	public void postRender(GameContainer container, PlayState game, Graphics g)
		throws SlickException {
		
	}
	
	/**
	 * @param container GameContainer
	 * @param game PlayState that is the parent of this level
	 * @param delta milliseconds since last loop
	 * @throws SlickException
	 */
	public void update(GameContainer container, PlayState game, int delta)
		throws SlickException {
		if (background != null)
			background.update(container, game, delta);
		if (updateMapLocation(delta)) {
			loadEvents(game);
		}
	}

	/**
	 * Loads the events for the current sy(row) location
	 * @param game PlayState that is the parent of this level
	 */
	private void loadEvents(PlayState game) {
		LinkedList<Event> list = events.get(sy);
		if (list != null) {
			for (Event event:list) {
				if (event.type.equalsIgnoreCase("cutscene")) {
					/*//TODO Get Character specific cut-scene if available.
					 * cut = event.properties.get(character.name);
					 * if (cut == null) {
					 * 	cut = event.properties.get(default);
					 * }
					 */
					Cutscene c = cutscenes.get(event.name);
					if (c != null)
						game.startCutscene(c);
				} else if (event.type.equalsIgnoreCase("enemy")) {
					game.addActor(createActor(game, event));
				} else if (event.type.equalsIgnoreCase("speedChange")) {
					this.setSpeed(Float.parseFloat(event.properties.get("speed")));
				} else if (event.type.equalsIgnoreCase("points")||event.type.equalsIgnoreCase("weapon")) {
					game.addActor(ActorFactory.createPickup(event));
				} else if (event.type.equalsIgnoreCase("deco")) {
					game.addActor(ActorFactory.createDeco(event));
				}
			}
		}
	}
	
	/**
	 * 
	 * @param game PlayState that is the parent of this level
	 * @param event An Event that contains the properties for an Actor
	 * @return Actor that is defined in the event
	 */
	private Actor createActor(PlayState game, Event event) {
		Actor actor = null;
		if (event.name.equalsIgnoreCase("enemy")) {
			actor = ActorFactory.createEnemy(event);
		} else if (event.name.equalsIgnoreCase("ground")) {
			actor = ActorFactory.createGroundActor(event);
		}
		
		return actor;
	}
	
	/**
	 * @param delta milliseconds since last loop
	 * @return true if a new row of tiles has been loaded
	 */
	private boolean updateMapLocation(int delta) {
		this.y += speed * delta;
		
		//If end of map and not looped
		if (sy <= 0 && this.y >= 0 && !looped) {
			this.y = 0;
			this.setSpeed(0);
			return false;
		}
		
		//set to render next layer of tiles if map has scrolled down enough
		if (this.y >= 0) {
			sy--;
			if (sy < 0 && looped) {
				sy = this.getHeight() - height; //makes map loop
			}
			this.y -= this.getTileHeight();
			return true;
		}
		return false;
	}

	/**
	 * @return the speed
	 */
	public float getSpeed() {
		return speed;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return width of map in pixels
	 */
	public int getWidth() {
		return super.getWidth() * getTileWidth();
	}
	
	public boolean isFinished() {
		if (sy <= 0 && this.y >= 0 && !looped) return true;
		return false;
	}

	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(float speed) {
		this.speed = speed;
	}

	/**
	 * @param background the background to set
	 */
	public void setBackground(Background background) {
		this.background = background;
	}
	
	/**
	 * @param cutscene the Cutscene to add
	 */
	public void addCutscene(Cutscene cutscene) {
		cutscenes.put(cutscene.getName(), cutscene);
	}
	
	/**
	 * An abstraction of the Objects defined within a TiLED map
	 * @author Affian
	 */
	public class Event {
		int id;
		int groupId;
		float y;
		float x;
		String name;
		String type;
		HashMap<String, String> properties;
		
		/**
		 * @param id event ID
		 * @param groupId ID of the object group this event belongs to
		 * @param y Y Tile location of this event
		 * @param x X Tile location of this event
		 * @param name Name of the event
		 * @param type Type of the event
		 */
		public Event(int id, int groupId, int y, int x, String name, String type) {
			this.id = id;
			this.groupId = groupId;
			this.y = y;
			this.x = x;
			this.name = name;
			this.type = type;
			properties = new HashMap<String, String>();
		}
	}

}
