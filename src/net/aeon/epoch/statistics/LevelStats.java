/**
 * 
 */
package net.aeon.epoch.statistics;

/**
 * @author Affian
 *
 */
public class LevelStats implements Statistics {

	private String levelName;
	private int time = 0;
	private int score = 0;
	private int enemies = 0;
	private int kills = 0;
	private int bonus = 0;
	
	
	/**
	 * 
	 */
	public LevelStats(String name) {
		levelName = name;
	}
	
	public String getLevelName() {
		return levelName;
	}


	public int getTime() {
		return time;
	}


	public void incrementTime(int time) {
		this.time += time;
	}


	public int getScore() {
		return score;
	}


	public void setScore(int score) {
		this.score = score;
	}


	public int getEnemyCount() {
		return enemies;
	}


	public void addEnemy() {
		this.enemies++;
	}


	public int getKillCount() {
		return kills;
	}


	public void addKill() {
		this.kills++;
	}


	public int getBonus() {
		return bonus;
	}


	public void setBonus(int bonus) {
		this.bonus = bonus;
	}
	
	public float getKillRatio() {
		if (enemies == 0 || kills == 0) return 0;
		return (float)enemies / (float)kills;
	}

}
