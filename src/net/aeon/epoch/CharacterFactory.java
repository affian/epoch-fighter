/**
 * 
 */
package net.aeon.epoch;

import org.newdawn.slick.geom.Rectangle;

import net.aeon.epoch.actor.Player;
import net.aeon.epoch.component.BankingSprite;
import net.aeon.epoch.component.KeyboardController;
import net.aeon.epoch.weapon.Laser;
import net.aeon.epoch.weapon.PlasmaCannon;

/**
 * @author Affian
 *
 */
public class CharacterFactory {
	
	/**
	 * List of character names
	 */
	public enum Toon {
		Alpha,
		Beta,
		Gamma,
		Theta,
		Zeta
	};

	/**
	 * 
	 */
	private CharacterFactory() {
		
	}
	
	/**
	 * 
	 * @param name The name of the character to load
	 * @return A Player Actor configured for the given Character
	 */
	public static Player getActor(Toon name) {
		Player player = null;
		
		
		switch (name) {
		case Alpha:
			player = new Player(Toon.Alpha.toString(), new PlasmaCannon());
			player.setSprite(new BankingSprite(player, ResourceManager.getInstance().getSpriteSheet("talon")));
			player.setHitBox(new Rectangle(0, 0, 12, 17));
			break;
		case Beta:
			player = new Player(Toon.Beta.toString(), new PlasmaCannon());
			player.setSprite(new BankingSprite(player, ResourceManager.getInstance().getSpriteSheet("stalker")));
			player.setHitBox(new Rectangle(0, 0, 12, 17));
			break;
		case Gamma:
			player = new Player(Toon.Gamma.toString(), new PlasmaCannon());
			player.setSprite(new BankingSprite(player, ResourceManager.getInstance().getSpriteSheet("shadow")));
			player.setHitBox(new Rectangle(0, 0, 12, 17));
			break;
		case Theta:
			player = new Player(Toon.Theta.toString(), new Laser());
			player.setSprite(new BankingSprite(player, ResourceManager.getInstance().getSpriteSheet("silver")));
			player.setHitBox(new Rectangle(0, 0, 12, 17));
			break;
		case Zeta:
			player = new Player(Toon.Zeta.toString(), new PlasmaCannon());
			player.setSprite(new BankingSprite(player, ResourceManager.getInstance().getSpriteSheet("carrot")));
			player.setHitBox(new Rectangle(0, 0, 12, 17));
			break;
		}
		
		player.setController(new KeyboardController(player));
		return player;
	}

}
