/**
 * 
 */
package net.aeon.epoch;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * @author Affian
 *
 */
public class EpochFighter extends StateBasedGame {
	
	public static final int PLAYSTATE = 0;
	public static final int MAINMENUSTATE = 2;


	/**
	 * 
	 */
	public EpochFighter() {
		super("Epoch Fighter");
		
		
		this.addState(new MenuState(MAINMENUSTATE));
		this.enterState(MAINMENUSTATE);
		this.addState(new PlayState(PLAYSTATE));

		
	}

	/* (non-Javadoc)
	 * @see org.newdawn.slick.state.StateBasedGame#initStatesList(org.newdawn.slick.GameContainer)
	 */
	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		//gc.setShowFPS(false);
		//gc.setVSync(true);
		//gc.setMultiSample(2);
		//gc.setClearEachFrame(false);
		//This is given in example code but it seems to be called elsewhere
		//this.getState(PLAYSTATE).init(gc, this);

	}

	/**
	 * @param args
	 * @throws SlickException 
	 */
	public static void main(String[] args) throws SlickException {
		AppGameContainer app = new AppGameContainer(new EpochFighter());
		
        app.setDisplayMode(800, 600, false);
        app.start();

	}

}
