/**
 * 
 */
package net.aeon.epoch.menu;

import java.util.ArrayList;

import net.aeon.epoch.CharacterFactory;
import net.aeon.epoch.EpochFighter;
import net.aeon.epoch.MenuHandler;
import net.aeon.epoch.PlayState;
import net.aeon.epoch.ResourceManager;
import net.aeon.epoch.CharacterFactory.Toon;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

/**
 * @author Affian
 *
 */
public class CharSelectMenu implements Menu {
	
	private static final int DEFAULT_SELECT = 2;
	private static final int THUMB_SPACE = 104;
	private static final float MIN_ALPHA = 0.3f;

	private MenuHandler handler;
	private int selected;
	private boolean chosen;
	private ArrayList<CharSelect> characters;
	
	//Spacings
	private float selectOffset;
	private float yOffset;
	
	//Images
	private Image background;
	private Image title;
	
	
	/**
	 * 
	 */
	public CharSelectMenu(MenuHandler handler) {
		this.handler = handler;
		this.selected = DEFAULT_SELECT;
		this.yOffset = 590 - 151; //590 - thumb height
		this.characters = new ArrayList<CharSelect>();
		this.selectOffset = THUMB_SPACE * selected;
		this.chosen = false;
		
		//Images
		try {
			characters.add(new CharSelect(
					new Image("/resources/images/char0-full.png"),
					new Image("/resources/images/mockCharThumb.png"),
					new Image("/resources/images/mockCharName.png")
			));
			characters.add(new CharSelect(
					new Image("/resources/images/char2-full.png"), 
					new Image("/resources/images/char2.png"), 
					new Image("/resources/images/testname.png")
			));
			characters.add(new CharSelect(
					new Image("/resources/images/char1-full.png"), 
					new Image("/resources/images/char1.png"), 
					new Image("/resources/images/testname.png")
			));
			characters.add(new CharSelect(
					new Image("/resources/images/char3-full.png"), 
					new Image("/resources/images/char3.png"), 
					new Image("/resources/images/testname.png")
			));
			characters.add(new CharSelect(
					new Image("/resources/images/char4-full.png"), 
					new Image("/resources/images/char4.png"), 
					new Image("/resources/images/testname.png")
			));
			
			background = ResourceManager.getInstance().getImage("characterSelectBackground");
			//borderLeft = new Image("/resources/images/borderleft.png");
			//borderRight = new Image("/resources/images/borderright.png");
			title = ResourceManager.getInstance().getImage("characterSelectTitle");
		} catch (SlickException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < characters.size(); i++) {
			if (i == selected)
				characters.get(i).thumb.setAlpha(1);
			else
				characters.get(i).thumb.setAlpha(MIN_ALPHA);
		}
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.menu.Menu#keyPressed(int, char)
	 */
	@Override
	public void keyPressed(int key, char c) {
		switch (key) {
		case Input.KEY_ESCAPE:
			handler.changeMenu("back");
			break;
		case Input.KEY_LEFT:
			if (selected > 0)
				selected--;
			break;
		case Input.KEY_RIGHT:
			if (selected < characters.size() - 1)
				selected++;
			break;
		case Input.KEY_ENTER:
				chosen = true;
		}

	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.menu.Menu#render(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame, org.newdawn.slick.Graphics)
	 */
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		CharSelect current = characters.get(selected);
		
		background.draw(0, 0);
		
		if (current.character != null)
			current.character.draw(50, 600 - current.character.getHeight());
		if (current.name != null)
			current.name.draw(207, 580 - current.thumb.getHeight() - current.name.getHeight());
		
		for (int i = 0; i < characters.size(); i++) {
			current = characters.get(i);
			
			current.thumb.draw((400 - current.thumb.getWidth() / 2) - selectOffset + (THUMB_SPACE * i), yOffset);
			
		}	
		
		//Border bars
		//borderLeft.draw(0, 0);
		//borderRight.draw(604, 0);
		
		title.draw(400 - (title.getWidth() / 2), 10);
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.menu.Menu#update(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame, int)
	 */
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		CharSelect current = null;
		int target = THUMB_SPACE * selected;
		float move = 0.5f * delta;
		
		if (chosen) {
			loadCharacter(game);
			
		}
		
		if (selectOffset > target) {
			//Move left
			if (selectOffset - move < target) {
				selectOffset = target;
			} else {
				selectOffset -= move;
			}
		} else if (selectOffset < target) {
			//Move right
			if (selectOffset + move > target) {
				selectOffset = target;
			} else {
				selectOffset += move;
			}
		}
		
		//Update Alpha
		for (int i = 0; i < characters.size(); i++) {
			current = characters.get(i);
			if (i == selected) {
				if (current.thumb.getAlpha() < 1)
					if (current.thumb.getAlpha() + 0.005f * delta > 1)
						current.thumb.setAlpha(1);
					else
						current.thumb.setAlpha(current.thumb.getAlpha() + 0.005f * delta);
			} else {
				if (current.thumb.getAlpha() > MIN_ALPHA)
					if (current.thumb.getAlpha() - 0.0005f * delta < 0.5f)
						current.thumb.setAlpha(MIN_ALPHA);
					else
						current.thumb.setAlpha(current.thumb.getAlpha() - 0.005f * delta);
			}
		}
		
	}
	
	/* (non-Javadoc)
	 * @see net.aeon.epoch.menu.Menu#enter(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame)
	 */
	@Override
	public void enter(GameContainer container, StateBasedGame game) {
		
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.menu.Menu#leave(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame)
	 */
	@Override
	public void leave(GameContainer container, StateBasedGame game) {
		selected = DEFAULT_SELECT;
		selectOffset = THUMB_SPACE * selected;
		chosen = false;
		
		for (int i = 0; i < characters.size(); i++) {
			if (i == selected)
				characters.get(i).thumb.setAlpha(1);
			else
				characters.get(i).thumb.setAlpha(MIN_ALPHA);
		}
	}

	private void loadCharacter(StateBasedGame game) {
		PlayState play = (PlayState)game.getState(EpochFighter.PLAYSTATE);
		switch (selected) {
		case 0:
			play.newGame(CharacterFactory.getActor(Toon.Alpha));
			break;
		case 1:
			play.newGame(CharacterFactory.getActor(Toon.Beta));
			break;
		case 2:
			play.newGame(CharacterFactory.getActor(Toon.Gamma));
			break;
		case 3:
			play.newGame(CharacterFactory.getActor(Toon.Theta));
			break;
		default:
			play.newGame(CharacterFactory.getActor(Toon.Zeta));
			break;
		}
		
		
		game.enterState(EpochFighter.PLAYSTATE, new FadeOutTransition(), new FadeInTransition());
		
	}

	/**
	 * 
	 * @author Affian
	 */
	private class CharSelect {
		public Image character;
		public Image thumb;
		public Image name;
		//public Image ship;
		
		public CharSelect(Image c, Image t, Image n) {
			character = c;
			thumb = t;
			name = n;
		}
	}

}
