/**
 * 
 */
package net.aeon.epoch.menu;

import net.aeon.epoch.MenuHandler;
import net.aeon.epoch.ResourceManager;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheetFont;
import org.newdawn.slick.state.StateBasedGame;

/**
 * @author Affian
 *
 */
public class MainMenu implements Menu {
	
	private static final String[] mainMenu = {
		"STORY MODE",
		"ARCADE MODE",
		"HIGHSCORES",
		"OPTIONS",
		"CREDITS",
		"EXIT"
	};
	
	private boolean exit;
	private MenuHandler handler;
	private int selected;
	private int menuMax;
	
	private Image background;
	
	private SpriteSheetFont menuFont;
	private SpriteSheetFont selectFont;
	
	private Sound cursorMove;
	private Sound cursorSelect;
	

	/**
	 * 
	 */
	public MainMenu(MenuHandler handler) {
		this.handler = handler;
		this.selected = 0;
		this.menuMax = 5;
		this.exit = false;

		//Images
		background = ResourceManager.getInstance().getImage("titlecard");
		
		//Fonts
		menuFont = ResourceManager.getInstance().getFont("kromasky");
		selectFont = ResourceManager.getInstance().getFont("kromagrad");
		
		//Sounds
		cursorMove = ResourceManager.getInstance().getSound("menu");
		cursorSelect = ResourceManager.getInstance().getSound("select");

	}


	/* (non-Javadoc)
	 * @see net.aeon.epoch.menu.Menu#keyPressed(int, char)
	 */
	@Override
	public void keyPressed(int key, char c) {
		switch (key) {
		case Input.KEY_ESCAPE:
			exit = true;
			break;
		case Input.KEY_UP:
			selected--;
			cursorMove.play();
			if (selected < 0) selected = menuMax;
			break;
		case Input.KEY_DOWN:
			selected++;
			cursorMove.play();
			if (selected > menuMax) selected = 0;
			break;
		case Input.KEY_ENTER:
			cursorSelect.play();
			select();
			break;
		default:
			break;
		}

	}
	
	private void select() {
		switch (selected) {
		case 0: //Story Mode
			handler.changeMenu("charSelect");
			break;
		case 1: //Arcade Mode
			break;
		case 2: //Options
			break;
		case 3: //Highscores
			break;
		case 4: //Credits
			handler.changeMenu("credits");
			break;
		case 5: //Exit
			exit = true;
			break;
		default:
			break;
		}
		
	}


	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		background.draw(0, 0);
		for (int i = 0; i <= menuMax; i++) {
			if (i == selected) {
				menuFont.drawString(400 - (menuFont.getWidth("+" + mainMenu[i] + "+") / 2), 360 + (menuFont.getLineHeight() * i) + (10 * i), "+" + mainMenu[i] + "+");
			} else {
				selectFont.drawString(400 - (menuFont.getWidth(mainMenu[i]) / 2), 360 + (menuFont.getLineHeight() * i) + (10 * i), mainMenu[i]);
			}
		}
	}


	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		if (exit) container.exit();
	}


	@Override
	public void enter(GameContainer container, StateBasedGame game) {
		
	}


	@Override
	public void leave(GameContainer container, StateBasedGame game) {
		
	}

}
