/**
 * 
 */
package net.aeon.epoch.menu;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * @author Affian
 *
 */
public interface Menu {

	public void render(GameContainer container, StateBasedGame game, Graphics g)
		throws SlickException;
	public void update(GameContainer container, StateBasedGame game, int delta)
		throws SlickException;
	public void keyPressed(int key, char c);
	public void enter(GameContainer container, StateBasedGame game);
	public void leave(GameContainer container, StateBasedGame game);
	
}
