/**
 * 
 */
package net.aeon.epoch.menu;

import net.aeon.epoch.MenuHandler;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * @author Affian
 *
 */
public class CreditsMenu implements Menu {

	
	private MenuHandler handler;
	private float speed;
	private float yOffset;

	/**
	 * 
	 */
	public CreditsMenu(MenuHandler handler) {
		this.handler = handler;
		this.yOffset = 600;
		this.speed = 0.2f;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.menu.Menu#enter(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame)
	 */
	@Override
	public void enter(GameContainer container, StateBasedGame game) {
		//load credits file

	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.menu.Menu#keyPressed(int, char)
	 */
	@Override
	public void keyPressed(int key, char c) {
		switch (key) {
		case Input.KEY_ESCAPE:
			handler.changeMenu("back");
			break;
		}

	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.menu.Menu#leave(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame)
	 */
	@Override
	public void leave(GameContainer container, StateBasedGame game) {
		this.yOffset = 600;
		//close credits file

	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.menu.Menu#render(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame, org.newdawn.slick.Graphics)
	 */
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.menu.Menu#update(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame, int)
	 */
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		yOffset -= speed * delta;

	}
	
//	public JSONObject getJsonFromUri(String url) throws JSONException, IOException {
//		URLConnection conn = null;
//		DataInputStream data = null;
//		String line;
//		StringBuffer buf = new StringBuffer();
//
//		conn = new URL(url).openConnection();
//		conn.connect();
//
//		data = new DataInputStream(new BufferedInputStream(conn.getInputStream()));
//
//		while ((line = data.readLine()) != null) {
//			buf.append(line + "\n");
//		}
//
//		data.close();
//
//		return new JSONObject(buf.toString());
//	}

}
