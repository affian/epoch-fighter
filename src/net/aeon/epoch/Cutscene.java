/**
 * 
 */
package net.aeon.epoch;

import java.util.ArrayList;
import java.util.HashMap;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * @author Affian
 *
 */
public class Cutscene {
	
	//Constants
	private static final int TEXT_SPEED = 50;
	private static final String RESOURCE_FOLDER = "/resources/";
	//private static final int MAX_CHAR_PER_LINE; //use for limiting or formatting text per line
	
	//x,y coords set the "center" of the cutscene
	private static final int X_OFFSET = 400;
	private static final int Y_OFFSET = 300;
	
	//Scene settings
	private String name;
	private Image backdrop;
	private HashMap<String, Image> speakerImages;
	private ArrayList<Message> dialog;
	
	//Transient data
	private int currentMessage;
	private int timer;
	private int caretPosition;
	private boolean finished;
	
	public enum Side {
		LEFT,
		RIGHT,
	};

	/**
	 * @param name
	 */
	public Cutscene(String name) {
		this(name, null);
	}
	
	/**
	 * @param name
	 * @param backdrop
	 */
	public Cutscene(String name, Image backdrop) {
		this.name = name;
		this.backdrop = backdrop;
		this.speakerImages = new HashMap<String, Image>();
		this.dialog = new ArrayList<Message>();
		
		this.currentMessage = 0;
		this.timer = 0;
		this.caretPosition = 0;
		this.finished = false;
	}
	
	/**
	 * @param container
	 * @param game
	 * @param delta
	 * @throws SlickException
	 */
	public void update(GameContainer container, PlayState game, int delta)
		throws SlickException {
		if (caretPosition < dialog.get(currentMessage).text.length()) {
			timer -= delta;
			if (timer <= 0) {
				caretPosition++;
				timer = TEXT_SPEED;
			}
		}
		
	}
	
	/**
	 * @param container
	 * @param game
	 * @param g
	 * @throws SlickException
	 */
	public void render(GameContainer container, PlayState game, Graphics g)
		throws SlickException {
		Message current = dialog.get(currentMessage);
		
		if (current.speaker != null) {
			if (current.side == Side.LEFT) {
				speakerImages.get(current.speaker).draw(X_OFFSET - (backdrop.getWidth()/2), Y_OFFSET + backdrop.getHeight() - speakerImages.get(current.speaker).getHeight()); //Left
			} else { //Right, draw a flipped image
				speakerImages.get(current.speaker).getFlippedCopy(true, false).draw(X_OFFSET + (backdrop.getWidth()/2) - speakerImages.get(current.speaker).getWidth(), Y_OFFSET + backdrop.getHeight() - speakerImages.get(current.speaker).getHeight());
				//speakerImages.get(current.speaker).draw(X_OFFSET + (backdrop.getWidth()/2) - speakerImages.get(current.speaker).getWidth(), Y_OFFSET + backdrop.getHeight() - speakerImages.get(current.speaker).getHeight()); //Right
			}	
		}
		if (backdrop != null)
			backdrop.draw(X_OFFSET - (backdrop.getWidth()/2), Y_OFFSET);
		
		//for (int count = 0; count < caretPosition; count += 23) {
		//	
		//}
		//FIXME change to handle null backdrops
		int count = 0;
		while (count < caretPosition) {
			if (count < 23) {
				game.getFont().drawString(
						X_OFFSET - (backdrop.getWidth()/2) + 10, 
						Y_OFFSET + 10, 
						current.text.substring(0, Math.min(23, caretPosition)));
			} else {
				game.getFont().drawString(
						X_OFFSET - (backdrop.getWidth()/2) + 10, 
						Y_OFFSET + 10 + (game.getFont().getLineHeight() * (count / 23)), 
						current.text.substring(count, count + Math.min(23, caretPosition - count)));
			}
			count += 23;
		}
		
		//game.getFont().drawString(X_OFFSET - (backdrop.getWidth()/2) + 10, Y_OFFSET + 10, current.text.substring(0, caretPosition));
	}
	
	/**
	 * Advances the cutscene. Sets all text to display if it is still being 
	 * printed or advances to next message if all text has been displayed already
	 */
	public void advance() {
		if (caretPosition < dialog.get(currentMessage).text.length()) {
			caretPosition = dialog.get(currentMessage).text.length();
		} else {
			if (currentMessage < dialog.size() -1)
				currentMessage++;
			else {
				finished = true;
			}
			caretPosition = 0;
			timer = 0;
		}
	}
	
	/**
	 * @param speakerName the reference name of the speaker image
	 * @param speakerImage the Image of the speaker
	 */
	public void addSpeaker(String speakerName, Image speakerImage) {
		speakerImages.put(speakerName, speakerImage);
	}
	
	/**
	 * shortcut function to add a speaker. adds the speaker with the given
	 * reference and loads a PNG image with the same name in the /resource/ folder
	 * @param speaker the filename of the speakers image to add, excluding file extention
	 * @throws SlickException
	 */
	public void addSpeaker(String speaker) throws SlickException {
		speakerImages.put(speaker, new Image(RESOURCE_FOLDER + speaker + ".png"));
	}
	
	/**
	 * @param speaker reference to speaker image
	 * @param text dialog text
	 * @param side the Side on which to render the speaker
	 */
	public void addDialog(String speaker, String text, Side side) {
		dialog.add(new Message(speaker, text, side));
	}
	
	/**
	 * @return Name of cutscene
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return true if cutscene is finished
	 */
	public boolean isFinished() {
		return finished;
	}
	
	/**
	 * @param image the Image to set the text backdrop to
	 */
	public void setBackdrop(Image image) {
		this.backdrop = image;
	}

	/**
	 * Encapsulates each segment of dialog into a message which is stored in
	 * an ArrayList
	 * @author Affian
	 */
	private class Message {
		public String speaker;
		public String text;
		public Side side;
		
		/**
		 * @param spk reference to speaker image
		 * @param txt dialog text
		 * @param sd the Side on which to render the speaker
		 */
		public Message(String spk, String txt, Side sd) {
			speaker = spk;
			text = txt;
			side = sd;
		}
	}

}
