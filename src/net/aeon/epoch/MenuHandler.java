/**
 * 
 */
package net.aeon.epoch;

import org.newdawn.slick.state.transition.Transition;

/**
 * @author Affian
 *
 */
public interface MenuHandler {

	public void changeMenu(String newMenu);
	public void changeMenu(String newMenu, Transition leave, Transition enter);
	
}
