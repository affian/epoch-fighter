/**
 * 
 */
package net.aeon.epoch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import net.aeon.epoch.actor.Actor;
import net.aeon.epoch.actor.Enemy;
import net.aeon.epoch.actor.Player;
import net.aeon.epoch.statistics.LevelStats;
import net.aeon.epoch.ui.Hud;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheetFont;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.particles.ConfigurableEmitter;
import org.newdawn.slick.particles.ParticleSystem;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

/**
 * @author Affian
 *
 */
public class PlayState extends BasicGameState {
	
	/** The amount of play area bleed before an entity gets deleted */
	public static final int PLAY_MARGIN = 80;
	/** Left hand offset for the sidebar to the play area */
	public static final int X_OFFSET = 196;//197
	/** Maximum score multiplier */
	public static final int MAX_MULTI = 10;
	/** Timeout for kill combos in milliseconds */
	public static final int COMBO_TIMEOUT = 800;
	/** Relative location of level files */
	private static final String LEVEL_DIR = "/resources/level/";
	
	//Static attributes
	private int id;
	private Hud hud;
	private Level level;
	private Player player;
	private Rectangle playArea;
	private SpriteSheetFont font;
	private ResourceManager resourceManager;
	private ParticleSystem ps;
	private LinkedList<String> levelList;
	
	//Actor lists
	/** Actors in this list are added at the end of the update call, before the delete list is called */
	private ArrayList<Actor> add;
	/** Actors in this list are removed from the actor list after the add list is added*/
	private ArrayList<Actor> delete;
	/** The current list of actors in play, called for both updates and rendering */
	private ArrayList<Actor> actors;
	
	//Transient data
	private long timer;
	private int comboTimer;
	private float multiplier;
	private Cutscene cutscene;
	private GameState gameState;
	private GameState previousState;
	private LevelStats levelStats;
	
	//Music music;
	
	public enum GameState {
		PLAYING,
		PAUSED,
		/** depreciated */
		STARTING,
		GAME_OVER,
		CUTSCENE,
		FINISHED,
		/** unused */
		MENU,
	};

	/**
	 * @param id State ID
	 */
	public PlayState(int id) {
		timer = 0;
		multiplier = 1;
		comboTimer = 0;
		this.id = id;
		actors = new ArrayList<Actor>();
		delete = new ArrayList<Actor>();
		add = new ArrayList<Actor>();
		this.setGameState(GameState.PLAYING);
		cutscene = null;
		level = null;
		levelList = null;
	}
	
	/**
	 * Resets all variables so that a new level can be intialised
	 */
	private void reset() {
		multiplier = 1;
		comboTimer = 0;
		actors.clear();
		delete.clear();
		add.clear();
		this.setGameState(GameState.PLAYING);
		cutscene = null;
		level = null;
		ps.removeAllEmitters();
		hud.reset();
	}

	/* (non-Javadoc)
	 * @see org.newdawn.slick.state.BasicGameState#getID()
	 */
	@Override
	public int getID() {
		return id;
	}

	/* (non-Javadoc)
	 * @see org.newdawn.slick.state.GameState#init(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame)
	 */
	@Override
	public void init(GameContainer arg0, StateBasedGame arg1)
			throws SlickException {
		resourceManager = ResourceManager.getInstance();
		hud = new Hud();
		font = resourceManager.getFont("default");
		ps = new ParticleSystem("/resources/images/explode.png");
		
		
	}
	
	/* (non-Javadoc)
	 * @see org.newdawn.slick.state.BasicGameState#keyPressed(int, char)
	 */
	public void keyPressed(int key, char c) {
		if ((key == Input.KEY_P || key == Input.KEY_PAUSE) && (gameState == GameState.PLAYING || gameState == GameState.PAUSED)) {
			if (gameState == GameState.PLAYING)
				setGameState(GameState.PAUSED);
			else
				setGameState(GameState.PLAYING);
		} else { //Any key
			switch (gameState) {
			case CUTSCENE:
				if (cutscene != null) cutscene.advance();
				break;
			case FINISHED:
				nextLevel();
				break;
			}
		}

	}

	/* (non-Javadoc)
	 * @see org.newdawn.slick.state.GameState#render(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame, org.newdawn.slick.Graphics)
	 */
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		//g.scale(1.28f, 1.28f); //ups the res from 800x600 to 1024x768
		
		level.preRender(container, this, g);//layers under the actors
		for (Actor actor:actors) actor.render(container, this, g);
		ps.render();
		player.render(container, this, g);
		level.postRender(container, this, g);//layers over the actors
		hud.render(container, this, g);
		
		/*=== Uncomment to Draw Hit boxes ===*/
		//for (Actor actor: actors) if (actor.getHitBox() != null) g.draw(actor.getHitBox());
		
		 switch(gameState) {
		 case MENU:
			 break;
		 case CUTSCENE:
			if (cutscene != null)
				cutscene.render(container, this, g);
		 	break;
		 case GAME_OVER:
			//Draw game over overlay
			break;
		 case FINISHED:
			break;
		 case PAUSED:
			break;
		 default:
			break;
		 }
		 
		 
		 
	}

	/* (non-Javadoc)
	 * @see org.newdawn.slick.state.GameState#update(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame, int)
	 */
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		
		timer += delta;
		
		if (gameState == GameState.PLAYING && !container.hasFocus()) {
			this.setGameState(GameState.PAUSED);
		}
		
		switch (gameState) {
		case PLAYING:
			updatePlaying(container, delta);
			break;
		case CUTSCENE:
			if (cutscene != null && !cutscene.isFinished()) {
				cutscene.update(container, this, delta);
			} else {	
				setGameState(previousState);
				cutscene = null;
			}
			break;
		case GAME_OVER:
			level.update(container, this, delta);
			for (Actor actor:actors) {
				actor.update(container, this, delta);
			}
			ps.update(delta);
			break;
		case FINISHED:
			ps.update(delta);
			break;
		default:
			break;
		}

		//TODO better key handling
		if (container.getInput().isKeyPressed(Input.KEY_ESCAPE)) {
			//music.fade(300, 0, true);
			game.enterState(EpochFighter.MAINMENUSTATE, new FadeOutTransition(), new FadeInTransition());
		}
			
		hud.update(container, this, delta);
		
	}

	/**
	 * Update actions while in the PLAYING game state
	 * @param container
	 * @param delta
	 * @throws SlickException
	 */
	private void updatePlaying(GameContainer container, int delta)
			throws SlickException {
		level.update(container, this, delta);
		player.update(container, this, delta);
		
		levelStats.incrementTime(delta);
		
		checkPlayerBounds();
		updatePlayAreaLocation();
		for (Actor actor:actors) {
			actor.update(container, this, delta);
		}
		for (Actor actor:actors) {
			//Check if actor has moved out of bounds
			if (!playArea.contains(actor.getAbsoluteX(this), actor.getAbsoluteY(this))) {
				delete.add(actor);
			} else if(player.checkCollision(this, actor)) { //check if actor is colliding with player
				actor.checkCollision(this, player);
			} else { //check the rest of the actors for collisions
				for (Actor other: actors) {
					if (actor.checkCollision(this, other))
						break;
				}
			}
			//TODO player.die / Game over
			//	player explodes, after exploding:
			//	stop updating(?), play game over music/animation
			//	display gameover overlay, wait for any key press to go to high scores/menu
		}
		for (int i = 0; i < ps.getEmitterCount(); i++) {
			ps.moveAll(ps.getEmitter(i), 0, level.getSpeed() * delta);
			ConfigurableEmitter foo = (ConfigurableEmitter)ps.getEmitter(i);
			foo.setPosition(foo.getX(), foo.getY() + level.getSpeed() * delta);
		}
		
		actors.addAll(add);
		actors.removeAll(delete);
		delete.clear();
		add.clear();
		
		if (comboTimer > 0) comboTimer -= delta;
		if (comboTimer <= 0) multiplier = 1;
		
		ps.update(delta);
		
		if (level.isFinished()) {
			setGameState(GameState.FINISHED);
		}
	}

	/**
	 * Constrain the player within the play area
	 */
	private void checkPlayerBounds() {
		float x = player.getAbsoluteX(this);
		float y = player.getAbsoluteY(this);
		boolean collision = false;
		
		//TODO Total mess, needs clean up - needs more flexible coding
		if (x > 604 - (player.getSprite().getWidth() / 2)) {
			x = 604 - (player.getSprite().getWidth() / 2);
			collision = true;
		} else if (x < X_OFFSET + (player.getSprite().getWidth() / 2)) {
			x = X_OFFSET + (player.getSprite().getWidth() / 2);
			collision = true;
		}
		if (y > 600 - player.getSprite().getHeight()) {
			y = 600 - player.getSprite().getHeight();
			collision = true;
		} else if (y < 0) {
			y = 0;
			collision = true;
		}
		if (collision) {
			player.getPosition().x = x - playArea.getX();
			player.getPosition().y = y - playArea.getY();
		}
		
	}
	
	/**
	 * Moves the play area left and right in relation to the players movement.
	 * Movement is based on the percentage of where the player is in relation
	 * to the min and max X coordinate and the map is updated to match the
	 * inverse of that percentage. So as the player moves left the map moves right.
	 */
	private void updatePlayAreaLocation() {
		float mapMinX = X_OFFSET + 328/*408*/ - level.getWidth();
		
		float realXOffset = X_OFFSET + (player.getSprite().getWidth() / 2);
		float playerX = player.getAbsoluteX(this) - realXOffset;
		
		float playerRange = (604 - (player.getSprite().getWidth() / 2)) - realXOffset;
		float mapRange = (X_OFFSET - PLAY_MARGIN) - mapMinX;
		float playerPer = (playerX / playerRange);
		
		playArea.setX(mapRange * (1 - playerPer) + mapMinX);
	}
	
	/* (non-Javadoc)
	 * @see org.newdawn.slick.state.BasicGameState#enter(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame)
	 */
	public void enter(GameContainer container, StateBasedGame game) 
		throws SlickException {
		if (level == null || player == null) {
			throw new RuntimeException("A level and player must be present before entering the PlayState");
		}
		//music = ResourceManager.getInstance().getMusic("Rock Garden");
		//music.play();
		
	}
	
	/* (non-Javadoc)
	 * @see org.newdawn.slick.state.BasicGameState#leave(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame)
	 */
	public void leave(GameContainer container, StateBasedGame game) 
		throws SlickException {
		reset();
	}

	public void newGame(Player player) {
		
		timer = 0;
		
		//TODO levellist = player.getLevellist
		levelList = new LinkedList<String>();
		levelList.add("canyon");
		levelList.add("approach");
		levelList.add("test");
		//----------------------
		
		this.setPlayer(player);
		this.nextLevel();
		
	}

	@SuppressWarnings("unused")
	private void gameOver() {
		this.setGameState(GameState.GAME_OVER);
		//change music
		
	}

	/**
	 * @param ref the reference to the TilED level file
	 */
	public void loadLevel(String ref) {
		try {
			this.level = new Level(ref);
		} catch (SlickException e) {
			e.printStackTrace();
		}
		levelStats = new LevelStats(level.getName());
		playArea = new Rectangle(400 - ((level.getWidth() / 2) + PLAY_MARGIN), 0 - PLAY_MARGIN, level.getWidth() + (PLAY_MARGIN * 2), 600 + (PLAY_MARGIN * 2));
	}

	private void nextLevel() {
		
		this.reset();
		//TODO add exception case for no next level
		
		this.loadLevel(LEVEL_DIR + levelList.pop() + ".tmx");
		player.setPosition(new Vector2f(playArea.getWidth() / 2, 550 + PLAY_MARGIN));
		this.setGameState(GameState.PLAYING);
		
	}

	/**
	 * @param cutscene the Cutscene to play
	 */
	public void startCutscene(Cutscene cutscene) {
		this.cutscene = cutscene;
		setGameState(GameState.CUTSCENE);
	}

	/**
	 * @return the resourceManager
	 */
	public ResourceManager getResourceManager() {
		return resourceManager;
	}

	/**
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * @return the font
	 */
	public SpriteSheetFont getFont() {
		return font;
	}
	
	/**
	 * @return the gameState
	 */
	public GameState getGameState() {
		return gameState;
	}
	
	/**
	 * @return the timer
	 */
	public long getTimer() {
		return timer;
	}

	/**
	 * @return the actors
	 */
	public ArrayList<Actor> getActors() {
		return actors;
	}

	/**
	 * @return the playArea
	 */
	public Rectangle getPlayArea() {
		return playArea;
	}

	/**
	 * @return the previousState
	 */
	public GameState getPreviousState() {
		return previousState;
	}

	/**
	 * @return the level
	 */
	public Level getLevel() {
		return level;
	}

	/**
	 * @return the multiplier
	 */
	public float getMultiplier() {
		return multiplier;
	}

	/**
	 * @return the levelStats
	 */
	public LevelStats getLevelStats() {
		return levelStats;
	}

	/**
	 * @param previousState the previousState to set
	 */
	private void setPreviousState(GameState previousState) {
		this.previousState = previousState;
	}

	/**
	 * @param player the player to set
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	/**
	 * @param gs the gameState to set
	 */
	public void setGameState(GameState gs) {
		this.setPreviousState(this.gameState);
		this.gameState = gs;
	}

	/**
	 * @param levelList the levelList to set
	 */
	public void setLevelList(LinkedList<String> levelList) {
		this.levelList = levelList;
	}

	/**
	 * @param actor the Actor to add
	 */
	public void addActor(Actor actor) {
		if (actor instanceof Enemy) levelStats.addEnemy();
		add.add(actor);
	}
	
	/**
	 * @param newActors a Collection of Actors to add
	 */
	public void addActors(Collection<? extends Actor> newActors) {
		add.addAll(newActors);
	}
	
	/**
	 * @param actor the Actor to kill
	 */
	public void killActor(Actor actor) {
		if (actor instanceof Enemy) {
			Enemy enemy = (Enemy)actor;
			levelStats.addKill();
			
			player.setScore(player.getScore() + (int)(enemy.getValue() * multiplier));
			if (comboTimer > 0 && multiplier <= MAX_MULTI) {
				multiplier += 0.1f;
			}
			comboTimer = COMBO_TIMEOUT;
		}
		if (actor instanceof Player) {
			//player explodes
			//set game over state
			//gameOver();
			
			return;
		}

		ConfigurableEmitter ce = null;
			
		ce = actor.getEmitter();
		if (ce != null) {
			ce.setPosition(actor.getAbsoluteX(this), actor.getAbsoluteY(this));
			ps.addEmitter(ce);
		}
		
		delete.add(actor);
	}
	

}
