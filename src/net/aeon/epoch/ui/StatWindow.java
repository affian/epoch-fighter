/**
 * 
 */
package net.aeon.epoch.ui;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.ResourceManager;
import net.aeon.epoch.statistics.LevelStats;

import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

/**
 * @author Affian
 *
 */
public class StatWindow {

	Image frame;
	LevelStats levelStats;
	Font font;
	
	public StatWindow(Hud hud, PlayState game) {
		levelStats = game.getLevelStats();
		font = ResourceManager.getInstance().getFont("default");
	}

	public void render(GameContainer container, PlayState game, Graphics g) {
		int screenCenter = container.getWidth() / 2;
		
		font.drawString(screenCenter - font.getWidth("Kills: "), 230, "Kills: " + levelStats.getKillCount());
		font.drawString(screenCenter - font.getWidth("Enemies: "), 200, "Enemies: " + levelStats.getEnemyCount());
		
	}

	public void update(GameContainer container, PlayState game, int delta) {
		// TODO Auto-generated method stub
		
	}

}
