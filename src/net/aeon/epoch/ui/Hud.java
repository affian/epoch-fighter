/**
 * 
 */
package net.aeon.epoch.ui;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.ResourceManager;
import net.aeon.epoch.PlayState.GameState;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheetFont;

/**
 * @author Affian
 *
 */
public class Hud {

	private Image borderLeft;
	private Image borderRight;
	private Image gameOver;
	
	private StatWindow statHud;

	/**
	 * 
	 */
	public Hud() {
		gameOver = ResourceManager.getInstance().getImage("gameover");
		borderLeft = ResourceManager.getInstance().getImage("borderleft");
		borderRight = ResourceManager.getInstance().getImage("borderright");
	}

	/**
	 * 
	 * @param container
	 * @param game
	 * @param g
	 * @throws SlickException
	 */
	public void render(GameContainer container, PlayState game, Graphics g)
			throws SlickException {
		SpriteSheetFont font = game.getFont();
		
		//Border bars
		borderLeft.draw(0, 0);
		borderRight.draw(604, 0);
		
		//Score
		String score = String.valueOf(game.getPlayer().getScore());
		if (score.length() < 8) {
			String prefix = "";
			int i = 8 - score.length();
			while (i-- > 0) {
				prefix += "0";
			}
			score = prefix + score;
		}
		font.drawString(206, 30, score);
		
		//Lives
		font.drawString(594 - font.getWidth("Lives 00"), 30, "Lives " + game.getPlayer().getLives());
		
		//Game Over
		if (game.getGameState() == GameState.GAME_OVER) {
			gameOver.draw(400 - gameOver.getWidth() / 2, 300 - gameOver.getHeight() / 2);
		}
		 
		//level
		font.drawString(400 - (font.getWidth(game.getLevel().getName())/2), 10, game.getLevel().getName(), Color.cyan);	
		
		//TODO remove debug code
		//font.drawString(10, 574, "Actors " + game.getActors().size());
		/*--------------------*/
		
		switch (game.getGameState()) {
		case PLAYING:
			break;
		case PAUSED:
			font.drawString(400 - (font.getWidth("Paused")/2), 250, "Paused", Color.red);
			break;
		case CUTSCENE:
			break;
		case FINISHED:
			if (statHud != null) statHud.render(container, game, g);
			else displayStatWindow(game);
			break;
		case GAME_OVER:
			gameOver.draw(400 - gameOver.getWidth() / 2, 300 - gameOver.getHeight() / 2);
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 * @param container
	 * @param game
	 * @param delta
	 * @throws SlickException
	 */
	public void update(GameContainer container, PlayState game, int delta)
			throws SlickException {
		if (statHud != null) statHud.update(container, game, delta);
	}
	
	public void displayStatWindow(PlayState game) {
		statHud = new StatWindow(this, game);
	}
	
	public void reset() {
		
	}
}
