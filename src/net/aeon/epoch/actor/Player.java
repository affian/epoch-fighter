/**
 * 
 */
package net.aeon.epoch.actor;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.weapon.PlasmaCannon;
import net.aeon.epoch.weapon.Weapon;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.particles.ConfigurableEmitter;

/**
 * @author Affian
 *
 */
public class Player extends Actor {
	
	private String name;
	private int score;
	private int lives;
	private int level;
	private Weapon weapon;
	

	/**
	 * 
	 */
	public Player() {
		this("default", new PlasmaCannon());
	}
	
	public Player(String name, Weapon weapon) {
		super();
		this.setName(name);
		this.setScore(0);
		this.setLives(3);
		this.setLevel(1);
		this.setWeapon(weapon);
		
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @return the lives
	 */
	public int getLives() {
		return lives;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @return the weapon
	 */
	public Weapon getWeapon() {
		return weapon;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param weapon the weapon to set
	 */
	public void setWeapon(Weapon weapon) {
		if (this.weapon != null && this.getWeapon().getClass().isInstance(weapon)) {
			this.getWeapon().incrementLevel();
		} else {
			this.weapon = weapon;
		}
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * @param lives the lives to set
	 */
	public void setLives(int lives) {
		this.lives = lives;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @param game
	 * @param delta
	 */
	public void attack(PlayState game, int delta) {
		weapon.fire(game, delta);
		
	}
	
	/* (non-Javadoc)
	 * @see net.aeon.epoch.Actor#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(GameContainer container, PlayState game, int delta)
		throws SlickException {
		super.update(container, game, delta);
		weapon.update(game, delta);
	}
	
	/* (non-Javadoc)
	 * @see net.aeon.epoch.Actor#checkCollision(net.aeon.epoch.PlayState, net.aeon.epoch.Actor)
	 */
	@Override
	public boolean checkCollision(PlayState game, Actor other) {
		if (other instanceof Bullet && other.getTeam() == Team.ENEMY && super.checkCollision(game, other)) {
			//lives--;
			//if (lives <= 0)
			//	game.killActor(this);
			return true;
		} else if (other instanceof Enemy && super.checkCollision(game, other)) {
			
		} else if (other instanceof Pickup && super.checkCollision(game, other)) {
			return true;
		}
		return false;
		
	}

	@Override
	public ConfigurableEmitter getEmitter() {
		// TODO Auto-generated method stub
		return null;
	}

}
