/**
 * 
 */
package net.aeon.epoch.actor;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.weapon.Weapon;

import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.particles.ConfigurableEmitter;

/**
 * @author Affian
 *
 */
public class WeaponPickup extends Pickup {
	
	private Weapon weapon;

	public WeaponPickup(Vector2f position) {
		super(position);
		this.weapon = null;
	}
	
	/**
	 * @param position the position of the WeaponPickup
	 * @param weapon the weapon that this pickup contains
	 */
	public WeaponPickup(Vector2f position, Weapon weapon) {
		super(position);
		this.setWeapon(weapon);
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.actor.Pickup#activate(net.aeon.epoch.PlayState, net.aeon.epoch.actor.Player)
	 */
	@Override
	protected void activate(PlayState game, Player player) {
		player.setWeapon(weapon);
	}

	@Override
	public ConfigurableEmitter getEmitter() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the weapon
	 */
	public Weapon getWeapon() {
		return weapon;
	}

	/**
	 * @param weapon the weapon to set
	 */
	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
		this.setSprite(weapon.getSprite());
		this.setHitBox(new Rectangle(0, 0, this.getSprite().getWidth(), this.getSprite().getHeight()));
	}

}
