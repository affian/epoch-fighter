/**
 * 
 */
package net.aeon.epoch.actor;

import net.aeon.epoch.PlayState;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.particles.ConfigurableEmitter;

/**
 * @author Affian
 *
 */
public class GroundActor extends Actor {

	/**
	 * @param position
	 * @param heading
	 */
	public GroundActor(Vector2f position, float heading) {
		super(position, heading);
		hp = 30;
		value = 10;
		centered = false;
		this.setTeam(Team.ENEMY);
	}
	
	public void update(GameContainer container, PlayState game, int delta)
		throws SlickException {
		super.update(container, game, delta);
		this.getPosition().y += game.getLevel().getSpeed() * delta;
	}
	
	public boolean checkCollision(PlayState game, Actor other) {
		if (other instanceof Bullet && other.getTeam() == Team.PLAYER && super.checkCollision(game, other)) {
			Bullet bullet = (Bullet)other;
			
			this.setHit(true);
			hp -= bullet.getPower();
			if (hp <= 0)
				game.killActor(this);
			return true;
		}
		
		return false;
	}

	@Override
	public ConfigurableEmitter getEmitter() {
		// TODO Auto-generated method stub
		return super.getEmitter();
	}

}
