/**
 * 
 */
package net.aeon.epoch.actor;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.component.Sprite;

import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.particles.ConfigurableEmitter;

/**
 * Generic bullet class
 * @author Affian
 */
public class Bullet extends Actor {
	
	private int power;

	/**
	 * 
	 */
	public Bullet() {
		this(new Vector2f(), 0, 1, null);
	}
	
	public Bullet(Bullet bullet) {
		this(new Vector2f(bullet.getPosition().x, bullet.getPosition().y), bullet.getHeading(), bullet.getPower(), bullet.getSprite());
	}
	
	/**
	 * @param position
	 * @param heading
	 * @param power
	 */
	public Bullet(Vector2f position, float heading, int power) {
		this(position, heading, 1, null);
	}
	
	/**
	 * @param position
	 * @param heading
	 * @param power
	 * @param sprite
	 */
	public Bullet(Vector2f position, float heading, int power, Sprite sprite) {
		super(position, heading);
		this.setPower(power);
		this.setSprite(sprite);
	}
	
	/**
	 * @param power the power to set
	 */
	public void setPower(int power) {
		this.power = power;
	}

	/**
	 * @return the power
	 */
	public int getPower() {
		return power;
	}
	
	/* (non-Javadoc)
	 * @see net.aeon.epoch.actor.Actor#checkCollision(net.aeon.epoch.PlayState, net.aeon.epoch.actor.Actor)
	 */
	public boolean checkCollision(PlayState game, Actor other) {
		if (!(other instanceof Bullet) && other.getTeam() == Team.ENEMY && this.getTeam() == Team.PLAYER && super.checkCollision(game, other)) {
			
			//Vector2f actV = this.getPosition();
			//Vector2f othV = other.getPosition();
			//float w = actV.x - othV.x;
			//float h = actV.y - othV.y;
			
			game.killActor(this);
			
			return true;
		} else if (other instanceof Player && this.getTeam() == Team.ENEMY && super.checkCollision(game, other)) {
			game.killActor(this);
			return true;
		}
		return false;
	}

	@Override
	public ConfigurableEmitter getEmitter() {
		// TODO Auto-generated method stub
		return null;
	}

}
