/**
 * 
 */
package net.aeon.epoch.actor;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.component.Controller;
import net.aeon.epoch.component.Sprite;

import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.particles.ConfigurableEmitter;

/**
 * @author Affian
 *
 */
public class Enemy extends Actor {

	
	
	/**
	 * 
	 */
	public Enemy() {
		super();
	}

	/**
	 * @param position
	 * @param heading
	 */
	public Enemy(Vector2f position, float heading) {
		super(position, heading);
		this.setTeam(Team.ENEMY);
		hp = 20;
		value = 10;
	}
	
	public Enemy(Vector2f position, Controller controller,/*Attack attack,*/ Sprite sprite, int hp, int value) {
		super(position, 180);
		this.setController(controller);
		this.setSprite(sprite);
		this.setHp(hp);
		this.setValue(value);
	}

	public boolean checkCollision(PlayState game, Actor other) {
		if (other instanceof Bullet && other.getTeam() == Team.PLAYER && super.checkCollision(game, other)) {
			Bullet bullet = (Bullet)other;
			
			this.setHit(true);
			hp -= bullet.getPower();
			if (hp <= 0)
				game.killActor(this);
			return true;
		}
		
		return false;
	}

	@Override
	public ConfigurableEmitter getEmitter() {
		return super.getEmitter();
	}

}
