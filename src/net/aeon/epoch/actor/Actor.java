/**
 * 
 */
package net.aeon.epoch.actor;

import java.io.IOException;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.component.AnimatedSprite;
import net.aeon.epoch.component.Attack;
import net.aeon.epoch.component.Controller;
import net.aeon.epoch.component.ImageSprite;
import net.aeon.epoch.component.Sprite;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.particles.ConfigurableEmitter;
import org.newdawn.slick.particles.ParticleIO;

/**
 * @author Affian
 *
 */
public abstract class Actor {
	
	private static final int HIT_FLASH_RATE = 30; //Duration of flash in milliseconds
	
	private Vector2f position;
	private float heading;
	private Shape hitBox;
	private Team team;
	private boolean invulnerable;
	protected int hit;
	protected boolean centered;
	protected int hp;
	protected int value;
	private Attack attack;
	
	private Sprite sprite;
	private Controller controller;
	
	public enum Team {
		PLAYER,
		ENEMY,
		NEUTRAL,
	}

	/**
	 * 
	 */
	public Actor() {
		this(new Vector2f(), 0);
		
	}
	
	/**
	 * @param position relative within the play area
	 * @param heading degrees from north
	 */
	public Actor(Vector2f position, float heading) {
		this.setPosition(position);
		this.setHeading(heading);
		controller = null;
		sprite = null;
		team = Team.NEUTRAL;
		setHit(false);
		invulnerable = false;
		hit = 0;
		centered = true;
		value = 0;
	}
	
	public ConfigurableEmitter getEmitter() {
		ConfigurableEmitter ce = null;
		try {
			ce = ParticleIO.loadEmitter("/resources/newExplosion.xml");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ce;
	}
	
	/**
	 * @param container
	 * @param game
	 * @param g
	 * @throws SlickException
	 */
	public void render(GameContainer container, PlayState game, Graphics g)
			throws SlickException {
		if (sprite != null) {
			sprite.render(container, game, g);
		}
		//Debugging code to display hitboxes -This will adversely affect FPS!-
//		if (hitBox != null) {
//			//g.setColor(org.newdawn.slick.Color.green);
//			g.draw(hitBox);
//		}
		
	}
	
	/**
	 * @param container
	 * @param game
	 * @param delta
	 * @throws SlickException
	 */
	public void update(GameContainer container, PlayState game, int delta)
			throws SlickException {
		if (controller != null) controller.update(container, game, delta);
		if (attack != null) attack.update(container, game, delta);
		if (sprite != null) sprite.update(container, game, delta);
		if (hitBox != null) {
			if (centered) {
				this.hitBox.setCenterX(getAbsoluteX(game));
				this.hitBox.setCenterY(getAbsoluteY(game));
			} else {
				this.hitBox.setCenterX(getAbsoluteX(game) + (sprite.getWidth() / 2));
				this.hitBox.setCenterY(getAbsoluteY(game) + (sprite.getHeight() / 2));
			}
		}
		if (hit > 0) hit -= delta;
		if (hit < 0) hit = 0;
	}

	/**
	 * 
	 * @param game
	 * @return the X coordinate relative to the left of the screen
	 */
	public float getAbsoluteX(PlayState game) {
		return game.getPlayArea().getX() + position.x;
	}
	
	/**
	 * 
	 * @param game
	 * @return the Y coordinate relative to the top of the screen
	 */
	public float getAbsoluteY(PlayState game) {
		return game.getPlayArea().getY() + position.y;
	}
	
	/**
	 * @return the position
	 */
	public Vector2f getPosition() {
		return position;
	}

	/**
	 * @return the heading
	 */
	public float getHeading() {
		return heading;
	}

	/**
	 * @return the sprite
	 */
	public Sprite getSprite() {
		return sprite;
	}

	/**
	 * @return the controller
	 */
	public Controller getController() {
		return controller;
	}

	/**
	 * @return the hitBox
	 */
	public Shape getHitBox() {
		return hitBox;
	}

	/**
	 * @return the team
	 */
	public Team getTeam() {
		return team;
	}
	
	/**
	 * @return the hp
	 */
	public int getHp() {
		return hp;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @return the attack
	 */
	public Attack getAttack() {
		return attack;
	}

	/**
	 * @return the invulnerable
	 */
	public boolean isInvulnerable() {
		return invulnerable;
	}
	
	/**
	 * @return true if currently showing hit flash
	 */
	public boolean isHit() {
		if (hit > 0) return true;
		return false;
	}
	
	public boolean isCentered() {
		return centered;
	}

	/**
	 * @param attack the attack to set
	 */
	public void setAttack(Attack attack) {
		this.attack = attack;
	}

	/**
	 * @param hp the hp to set
	 */
	public void setHp(int hp) {
		this.hp = hp;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * @param set set to true on hit to start the collision flash
	 */
	public void setHit(boolean set) {
		if (set) {
			hit = HIT_FLASH_RATE;
		} else {
			hit = 0;
		}
	}

	/**
	 * @param invulnerable set to true to make this actor invulnerable
	 */
	public void setInvulnerable(boolean invulnerable) {
		this.invulnerable = invulnerable;
	}

	/**
	 * @param team the team to set
	 */
	public void setTeam(Team team) {
		this.team = team;
	}

	/**
	 * @param hitBox the hitBox to set
	 */
	public void setHitBox(Shape hitBox) {
		this.hitBox = hitBox;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(Vector2f position) {
		this.position = position;
	}

	/**
	 * Sets the direction (degrees from north) that the sprite should
	 * drawn at, also rotates the hit box to match.
	 * @param heading the heading to set
	 */
	public void setHeading(float heading) {
		this.heading = heading;
		if (hitBox != null) this.hitBox = hitBox.transform(Transform.createRotateTransform((float) java.lang.Math.toRadians(heading), position.x, position.y));
	}
	
	public void setSprite(Animation animation) {
		this.sprite = new AnimatedSprite(this, animation);
	}
	
	/**
	 * Shortcut method to create a new ImageSprite
	 * @param image the Image to use as a sprite
	 */
	public void setSprite(Image image) {
		this.sprite = new ImageSprite(this, image);
	}

	/**
	 * @param sprite the sprite to set
	 */
	public void setSprite(Sprite sprite) {
		this.sprite = sprite;
	}

	/**
	 * @param controller the controller to set
	 */
	public void setController(Controller controller) {
		this.controller = controller;
	}
	
	/**
	 * Used to check if the hit boxes of two Actors are intersecting
	 * Overridden by sub classes to check Actors type to see if it should collide
	 * then calls this method via super to do the actual hit box checking.
	 * 
	 * Each actor only needs to handle it's own side of the collision as the
	 * collision is checked from both sides.
	 * @param game
	 * @param other
	 * @return true if a collision has been detected otherwise false
	 */
	public boolean checkCollision(PlayState game, Actor other) {
		if (this.isInvulnerable()) return false;
		if (this.hitBox == null || other.getHitBox() == null) return false;
		return this.getHitBox().intersects(other.getHitBox());
	}


}
