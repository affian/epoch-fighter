/**
 * 
 */
package net.aeon.epoch.actor;

import net.aeon.epoch.PlayState;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

/**
 * @author Affian
 *
 */
public class LaserBullet extends Bullet {
	
	private Actor owner;
	private int time = 0;
	private int xOffset = 0;

	/**
	 * 
	 */
	public LaserBullet() {
		super();
	}

	/**
	 * @param position
	 * @param heading
	 * @param power
	 */
	public LaserBullet(Vector2f position, float heading, int power) {
		super(position, heading, power);
	}

	/**
	 * @param position
	 * @param heading
	 * @param power
	 * @param sprite
	 */
	public LaserBullet(Vector2f position, float heading, int power, Image sprite) {
		super(position, heading, power);
		this.setSprite(sprite);
	}
	
	/* (non-Javadoc)
	 * @see net.aeon.epoch.actor.Actor#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(GameContainer container, PlayState game, int delta) 
		throws SlickException {
		super.update(container, game, delta);
		if (owner != null) {
			this.getPosition().x = owner.getPosition().x + xOffset;
		}
		time += delta;
	}
	
	public void render(GameContainer container, PlayState game, Graphics g)
	throws SlickException {

		this.getSprite().getImage().draw((getAbsoluteX(game)-(this.getSprite().getWidth()/2)),(getAbsoluteY(game)-(this.getSprite().getHeight()/2)), new Color(time - 255, getPower() * 100, 255 - time));

	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(Actor owner) {
		this.owner = owner;
	}

	/**
	 * @return the owner
	 */
	public Actor getOwner() {
		return owner;
	}

	/**
	 * @param xOffset the xOffset to set
	 */
	public void setxOffset(int xOffset) {
		this.xOffset = xOffset;
	}

	/**
	 * @return the xOffset
	 */
	public int getxOffset() {
		return xOffset;
	}

}
