/**
 * 
 */
package net.aeon.epoch.actor.boss;

import org.newdawn.slick.geom.Vector2f;

import net.aeon.epoch.actor.Enemy;
import net.aeon.epoch.component.Controller;
import net.aeon.epoch.component.Sprite;

/**
 * @author Affian
 *
 */
public abstract class AbstractBoss extends Enemy {

	/**
	 * 
	 */
	public AbstractBoss() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param position
	 * @param heading
	 */
	public AbstractBoss(Vector2f position, float heading) {
		super(position, heading);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param position
	 * @param controller
	 * @param sprite
	 * @param hp
	 * @param value
	 */
	public AbstractBoss(Vector2f position, Controller controller,
			Sprite sprite, int hp, int value) {
		super(position, controller, sprite, hp, value);
		// TODO Auto-generated constructor stub
	}

}
