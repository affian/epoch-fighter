/**
 * 
 */
package net.aeon.epoch.actor;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.ResourceManager;

import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.particles.ConfigurableEmitter;

/**
 * @author Affian
 *
 */
public class PointsPickup extends Pickup {

	/**
	 * @param position
	 * @param value
	 */
	public PointsPickup(Vector2f position, int value) {
		super(position);
		
		this.setValue(value);
		
	}
	
	/**
	 * 
	 * @param position
	 */
	public PointsPickup(Vector2f position) {
		super(position);
	}

	/**
	 * Set the actors sprite based on the pickup's point value.
	 * Called from {@link #PointsPickup(Vector2f, int) PointsPickup(Vector2f, int)}
	 */
	private void loadSprite() {
		this.setSprite(ResourceManager.getInstance().getAnimation("1k-gem"));
		this.setHitBox(new Rectangle(0, 0, this.getSprite().getWidth(), this.getSprite().getHeight()));
	}

	/**
	 * @see net.aeon.epoch.actor.Pickup#activate(net.aeon.epoch.PlayState, net.aeon.epoch.actor.Player)
	 */
	@Override
	public void activate(PlayState game, Player player) {
		player.setScore(player.getScore() + value);
	}
	
	/* (non-Javadoc)
	 * @see net.aeon.epoch.actor.Actor#setValue(int)
	 */
	public void setValue(int value) {
		super.setValue(value);
		loadSprite();
	}

	@Override
	public ConfigurableEmitter getEmitter() {
		// TODO Auto-generated method stub
		return null;
	}

}
