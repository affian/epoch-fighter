/**
 * 
 */
package net.aeon.epoch.actor;

import net.aeon.epoch.PlayState;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

/**
 * Abstract class the defines generic behaviour that is common to all pickups.
 * activate() is implemented in subclasses to define what the pickup does.
 * @author Affian
 */
public abstract class Pickup extends Actor {
	
	/** Set speed for the pickup to travel at */
	private float speed = 0.06f;

	/**
	 * @param position
	 */
	public Pickup(Vector2f position) {
		super(position, 0);
	}
	
	/**
	 * @see net.aeon.epoch.actor.Actor#update(org.newdawn.slick.GameContainer, net.aeon.epoch.PlayState, int)
	 */
	public void update(GameContainer container, PlayState game, int delta)
		throws SlickException {
		super.update(container, game, delta);
		this.getPosition().y += speed * delta;
	}
	
	/**
	 * @see net.aeon.epoch.actor.Actor#checkCollision(net.aeon.epoch.PlayState, net.aeon.epoch.actor.Actor)
	 */
	public boolean checkCollision(PlayState game, Actor other) {
		if (other instanceof Player && super.checkCollision(game, other)) {
			this.activate(game, (Player)other);
			game.killActor(this);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Overridden by subclasses to define what the pickup does upon collection.
	 * @param game The game state
	 * @param player the Player collection this Pickup
	 */
	protected abstract void activate(PlayState game, Player player);

	/**
	 * @return the speed
	 */
	public float getSpeed() {
		return speed;
	}

	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(float speed) {
		this.speed = speed;
	}

}
