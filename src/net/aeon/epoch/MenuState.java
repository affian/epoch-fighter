/**
 * 
 */
package net.aeon.epoch;

import java.util.HashMap;
import java.util.LinkedList;

import net.aeon.epoch.menu.CharSelectMenu;
import net.aeon.epoch.menu.CreditsMenu;
import net.aeon.epoch.menu.MainMenu;
import net.aeon.epoch.menu.Menu;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.EmptyTransition;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.state.transition.Transition;

/**
 * MenuState acts as a container for menu navigation.
 * Menus are stored as a Stack so that each new navigation option pushes
 * a new menu onto the stack. Navigation can be reversed (with pop) using 
 * the "back" keyword in the changeMenu() method.
 * @author Affian
 */
public class MenuState extends BasicGameState implements MenuHandler {
	
	/*
	 * OPTIONS
	 * Fullscreen
	 * Master, Music, SFX Volume
	 * Key configuration
	 */
	
	/** The State's ID */
	private int id;
	/** A linked list acting as a stack to manage menu navigation */
	private LinkedList<Menu> menuStack;
	/** The list of possible menus */
	private HashMap <String, Menu> menus;

	//Transition data
	/** The transition being used to enter the menu */
	private Transition enterTransition;
	/** The transition being used to leave the menu */
	private Transition leaveTransition;
	/** The next menu to display after transition  */
	private String newMenu;
	
	/**
	 * @param id the ID of this state
	 */
	public MenuState(int id) {
		this.id = id;
		menuStack = new LinkedList<Menu>();
		menus = new HashMap<String, Menu>();
	}

	/* (non-Javadoc)
	 * @see org.newdawn.slick.state.BasicGameState#getID()
	 */
	@Override
	public int getID() {
		return id;
	}

	/* (non-Javadoc)
	 * @see org.newdawn.slick.state.GameState#init(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame)
	 */
	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		//Create Menus
		menus.put("mainMenu", new MainMenu(this));
		menus.put("charSelect", new CharSelectMenu(this));
		menus.put("credits", new CreditsMenu(this));
		
		//Push root menu
		menuStack.push(menus.get("mainMenu"));
	}

	/* (non-Javadoc)
	 * @see org.newdawn.slick.state.GameState#render(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame, org.newdawn.slick.Graphics)
	 */
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		
		if (leaveTransition != null) {
			leaveTransition.preRender(game, container, g);
		} else if (enterTransition != null) {
			enterTransition.preRender(game, container, g);
		}
		
		menuStack.peek().render(container, game, g);
		
		if (leaveTransition != null) {
			leaveTransition.postRender(game, container, g);
		} else if (enterTransition != null) {
			enterTransition.postRender(game, container, g);
		}
	}

	/* (non-Javadoc)
	 * @see org.newdawn.slick.state.GameState#update(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame, int)
	 */
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		
		
		if (leaveTransition != null) {
			leaveTransition.update(game, container, delta);
			if (leaveTransition.isComplete()) {
				if (newMenu.equalsIgnoreCase("back")) {
					menuStack.pop().leave(container, game);
				} else {
					if (menus.containsKey(newMenu)) {
						menuStack.push(menus.get(newMenu));
						menuStack.peek().enter(container, game);
					}
				}
				newMenu = null;
				leaveTransition = null;
			} else {
				return;
			}
		}
		
		if (enterTransition != null) {
			enterTransition.update(game, container, delta);
			if (enterTransition.isComplete()) {
				enterTransition = null;
			} else {
				return;
			}
		}
		
		menuStack.peek().update(container, game, delta);
	}
	
	/* (non-Javadoc)
	 * @see org.newdawn.slick.state.BasicGameState#keyPressed(int, char)
	 */
	public void keyPressed(int key, char c) {
		menuStack.peek().keyPressed(key, c);
	}

	/* (non-Javadoc)
	 * @see org.newdawn.slick.state.BasicGameState#enter(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame)
	 */
	@Override
	public void enter(GameContainer container, StateBasedGame game) {
		//TODO play music
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.newdawn.slick.state.BasicGameState#leave(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame)
	 */
	@Override
	public void leave(GameContainer container, StateBasedGame game) {
		while (menuStack.size() > 1)
			menuStack.pop().leave(container, game);
		//TODO stop music
	}

	/*
	 * (non-Javadoc)
	 * @see net.aeon.epoch.MenuHandler#changeMenu(java.lang.String)
	 */
	@Override
	public void changeMenu(String newMenu) {
		changeMenu(newMenu, new FadeOutTransition(Color.black, 200), new FadeInTransition(Color.black, 200));
	}

	/*
	 * (non-Javadoc)
	 * @see net.aeon.epoch.MenuHandler#changeMenu(java.lang.String, org.newdawn.slick.state.transition.Transition, org.newdawn.slick.state.transition.Transition)
	 */
	@Override
	public void changeMenu(String newMenu, Transition leave, Transition enter) {
		if (leave == null) {
			leave = new EmptyTransition();
		}
		if (enter == null) {
			enter = new EmptyTransition();
		}
		this.leaveTransition = leave;
		this.enterTransition = enter;
		
		this.newMenu = newMenu;
		if (newMenu == null) {
			throw new RuntimeException("No game state registered with the ID: "+id);
		}
		
	}
}
