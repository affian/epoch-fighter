/**
 * 
 */
package net.aeon.epoch.weapon;

import org.newdawn.slick.Image;

import net.aeon.epoch.PlayState;

/**
 * @author Affian
 */
public interface Weapon {
	
	/**
	 * @param game
	 */
	public void fire(PlayState game, int delta);
	
	/**
	 * @param game
	 * @param delta
	 */
	public void update(PlayState game, int delta);
	
	/**
	 * @return weapon level
	 */
	public int getLevel();
	
	/**
	 * @return new weapon level
	 */
	public int incrementLevel();
	
	/**
	 * resets weapon level to base
	 */
	public void reset();
	
	/**
	 * @return Image to use as pickup sprite
	 */
	public Image getSprite();
}
