/**
 * 
 */
package net.aeon.epoch.weapon;

import java.util.ArrayList;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.ResourceManager;
import net.aeon.epoch.actor.Bullet;
import net.aeon.epoch.actor.Actor.Team;
import net.aeon.epoch.component.LinearController;

import org.newdawn.slick.Image;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

/**
 * @author Affian
 *
 */
public class PlasmaCannon implements Weapon {
	
	private static final int MAX_LEVEL = 5;
	
	private int fireDelay;
	private int level;
	private int cooldown;

	@SuppressWarnings("unused")
	private Sound shoot;

	/**
	 * 
	 */
	public PlasmaCannon() {
		level = 1;
		cooldown = 0;
		fireDelay = 120;
		
//		shoot = ResourceManager.getInstance().getSound("shoot");


	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.weapon.Weapon#fire(net.aeon.epoch.PlayState)
	 */
	@Override
	public void fire(PlayState game, int delta) {
		if (cooldown <= 0) {
			ArrayList<Bullet> bullets = new ArrayList<Bullet>();
			SpriteSheet sprites = game.getResourceManager().getSpriteSheet("bullets");
			
			Vector2f center = new Vector2f(game.getPlayer().getPosition());
			
			center.y -= 10;
			bullets.add(createBullet(sprites, center, 5, 2, 3, 0.4f, 0));
			if (level > 1) {
				center.y += 5;
				center.x -= 12;
				bullets.add(createBullet(sprites, center, 4, 1, 3, 0.4f, 0));
				center.x += 24;
				bullets.add(createBullet(sprites, center, 4, 1, 3, 0.4f, 0));
			}
			if (level > 2) {
				center.x -= 2;
				bullets.add(createBullet(sprites, center, 4, 13, 14, 0.4f, 35));
				center.x -= 20;
				bullets.add(createBullet(sprites, center, 4, 12, 14, 0.4f, 325));
			}
			if (level > 3) {
				
			}
			if (level > 4) {
				
			}
			game.addActors(bullets);
//			shoot.play(1, 0.5f);
			cooldown = fireDelay;
		}

	}
	
	public void update(PlayState game, int delta) {
		if (cooldown > 0) cooldown -= delta;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.weapon.Weapon#getLevel()
	 */
	@Override
	public int getLevel() {
		return level;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.weapon.Weapon#incrementLevel()
	 */
	@Override
	public int incrementLevel() {
		if (level < MAX_LEVEL)
			level++;
		return level;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.weapon.Weapon#reset()
	 */
	@Override
	public void reset() {
		level = 1;
		cooldown = 0;
	}
	
	/**
	 * Helper function for creating a plasma bullet
	 * @param ss SpriteSheet
	 * @param pos Position of bullet
	 * @param p Power
	 * @param sx X index of sprite in SpriteSheet
	 * @param sy Y index of sprite in SpriteSheet
	 * @param s Speed
	 * @param h Heading
	 * @return bullet
	 */
	private Bullet createBullet(SpriteSheet ss, Vector2f pos, int p, int sx, int sy, float s, float h) {
		Bullet bullet = new Bullet();
		bullet.setPower(p);
		bullet.setTeam(Team.PLAYER);
		bullet.setHitBox(new Rectangle(0,0,10,10));
		bullet.setPosition(new Vector2f(pos));
		bullet.setHeading(h);
		bullet.setSprite(ss.getSubImage(sx, sy));
		bullet.setController(new LinearController(bullet, s, h));
		return bullet;
	}

	/**
	 * @return the sprite
	 */
	public Image getSprite() {
		return ResourceManager.getInstance().getSpriteSheet("pickups").getSprite(0,4);
	}

}
