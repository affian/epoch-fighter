/**
 * 
 */
package net.aeon.epoch.weapon;

import java.util.ArrayList;

import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

import net.aeon.epoch.PlayState;
import net.aeon.epoch.ResourceManager;
import net.aeon.epoch.actor.Bullet;
import net.aeon.epoch.actor.LaserBullet;
import net.aeon.epoch.actor.Actor.Team;
import net.aeon.epoch.component.LinearController;

/**
 * @author Affian
 *
 */
public class Laser implements Weapon {
	
	private static final int MAX_LEVEL = 5;
	
	private int fireDelay;
	private int level;
	private int cooldown;

	/**
	 * 
	 */
	public Laser() {
		level = 1;
		cooldown = 0;
		fireDelay = 12;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.weapon.Weapon#fire(net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void fire(PlayState game, int delta) {
		if (cooldown <= 0) {
			ArrayList<Bullet> bullets = new ArrayList<Bullet>();
			
			switch (level) {
			case 1:
				LaserBullet bullet = new LaserBullet();
				Vector2f center = new Vector2f(game.getPlayer().getPosition());
				
				center.y -= 10;
				bullet.setOwner(game.getPlayer());
				bullet.setPower(5);
				bullet.setTeam(Team.PLAYER);
				bullet.setSprite(game.getResourceManager().getImage("laser"));
				bullet.setHitBox(new Rectangle(0,0,12,14));
				bullet.setPosition(new Vector2f(center));
				bullet.setController(new LinearController(bullet, 0.9f, 0));
				bullets.add(bullet);
				
	
				bullet = new LaserBullet();
				bullet.setxOffset(-12);
				bullet.setOwner(game.getPlayer());
				bullet.setPower(1);
				bullet.setTeam(Team.PLAYER);
				bullet.setSprite(game.getResourceManager().getImage("laser"));
				bullet.setHitBox(new Rectangle(0,0,12,14));
				bullet.setPosition(new Vector2f(center));
				bullet.setController(new LinearController(bullet, 0.9f, 0));
				bullets.add(bullet);
				
				bullet = new LaserBullet();
				bullet.setxOffset(12);
				bullet.setOwner(game.getPlayer());
				bullet.setPower(1);
				bullet.setTeam(Team.PLAYER);
				bullet.setSprite(game.getResourceManager().getImage("laser"));
				bullet.setHitBox(new Rectangle(0,0,12,14));
				bullet.setPosition(new Vector2f(center));
				bullet.setController(new LinearController(bullet, 0.9f, 0));
				bullets.add(bullet);
				break;
			default:
				break;
			}
			game.addActors(bullets);
			//shoot.play(1, 0.5f);
			cooldown = fireDelay;
		}
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.weapon.Weapon#getLevel()
	 */
	@Override
	public int getLevel() {
		return level;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.weapon.Weapon#incrementLevel()
	 */
	@Override
	public int incrementLevel() {
		if (level < MAX_LEVEL)
			level++;
		return level;
	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.weapon.Weapon#reset()
	 */
	@Override
	public void reset() {
		level = 1;
		cooldown = 0;

	}

	/* (non-Javadoc)
	 * @see net.aeon.epoch.weapon.Weapon#update(net.aeon.epoch.PlayState, int)
	 */
	@Override
	public void update(PlayState game, int delta) {
		if (cooldown > 0) cooldown -= delta;
	}

	@Override
	public Image getSprite() {
		
		return ResourceManager.getInstance().getSpriteSheet("pickups").getSprite(3, 4);
	}

}
